package com.example.asus_pc.supermarkets.Tasks

import android.os.AsyncTask


class doAsyncParameterized<T>(val handler: () -> T? = { null }, val onPostExecuteHandler: (T?) -> Unit = {}) : AsyncTask<Void, Void, T?>() {

    init {
        execute()
    }

    override fun doInBackground(vararg params: Void?): T? {
        return handler()
    }

    override fun onPostExecute(result: T?) {
        super.onPostExecute(result)
        onPostExecuteHandler(result)
    }
}