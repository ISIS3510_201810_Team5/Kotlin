package com.example.asus_pc.supermarkets.Activities

import android.os.Bundle
import android.support.v4.app.Fragment
import com.example.asus_pc.supermarkets.Fragments.CallAssistanceFragment
import com.example.asus_pc.supermarkets.Interfaces.IRouteAssistance
import com.example.asus_pc.supermarkets.R

class RouteAssistanceActivity : EcoActivity(), IRouteAssistance {

    val manager = supportFragmentManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_route_assistance)
        val fragment = CallAssistanceFragment()
        fragment.listener = this
        replaceFragment(fragment)
        this.initializeTextToSpeech(this, {
            val text = getString(R.string.call_assistance_fragment_use_phone)
            speak(text)
        })
    }

    override fun onDestroy() {
        super.onDestroy()
        this.destroyTextToSpeech()
    }

    override fun onCancelAssistance() {
        this.finish()
    }

    // Helpers
    fun replaceFragment(fragment: Fragment) {
        val transaction = manager.beginTransaction();
        transaction.replace(R.id.fragmet_holder, fragment);
        transaction.commit();
    }
}