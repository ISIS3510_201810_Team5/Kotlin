package com.example.asus_pc.supermarkets.Models

class CalculatePositionQuery {
    val supermarket_id: String
    val nearby_beacons: Array<String>

    constructor(supermarket_id: String, nearby_beacons: Array<String>) {
        this.supermarket_id = supermarket_id
        this.nearby_beacons = nearby_beacons
    }
}