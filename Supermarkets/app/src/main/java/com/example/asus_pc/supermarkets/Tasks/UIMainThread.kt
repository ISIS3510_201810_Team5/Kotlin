package com.example.asus_pc.supermarkets.Tasks

import android.app.Activity
import android.content.Context
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso

object UIMainThread {
    fun setText(activity: Activity?, textView: TextView, value: String) {
        run(activity, {
            textView.text = value
        })
    }

    fun loadImage(activity: Activity?, imageUrl: String, imageView: ImageView) {
        run(activity, {
            Picasso.get().load(imageUrl).resize(40, 40).into(imageView)
        })
    }

    fun changeVisibility(activity: Activity?, view: View, newVisibility: Int) {
        run(activity, {
            if (view.visibility != newVisibility) {
                view.visibility = newVisibility
            }
        })
    }

    // Helpers
    fun run(activity: Activity?, handler: () -> Unit) {
        activity?.runOnUiThread(Runnable {
            handler()
        })
    }
}