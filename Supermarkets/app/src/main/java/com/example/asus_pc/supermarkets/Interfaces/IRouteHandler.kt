package com.example.asus_pc.supermarkets.Interfaces

import com.example.asus_pc.supermarkets.Helpers.RouteHandler

interface IRouteHandler {
    fun onStartedRoute()
    fun onFinishRoute(shouldFinishedRoute: Boolean = true)
    fun onChangedStatus(status: RouteHandler.STATUS)
    fun onLostConnectivity(status: RouteHandler.STATUS)
    fun onApproachingDestination(distance: Double)
}