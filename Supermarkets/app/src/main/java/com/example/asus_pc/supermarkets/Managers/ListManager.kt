package com.example.asus_pc.supermarkets.Managers

import android.content.Context
import com.example.asus_pc.supermarkets.Databases.ListDatabase
import com.example.asus_pc.supermarkets.Entities.ListEntity
import com.example.asus_pc.supermarkets.Tasks.doAsync
import com.example.asus_pc.supermarkets.Tasks.doAsyncParameterized

object ListManager {

    // API
    fun getLists(context: Context, supermarketId: String, onSuccess: (List<ListEntity>) -> Unit) {
        doAsync(handler = {
            val lists: List<ListEntity>? = ListDatabase.getInstance(context)?.listEntityDao()?.getAll(supermarketId)
            if (lists != null) {
                onSuccess(lists)
            }
        })
    }

    fun addList(context: Context, listEntity: ListEntity, onSuccess: () -> Unit, onFailure: () -> Unit) {
        doAsyncParameterized<Long>(handler = {
            listEntity.name = listEntity.name.toLowerCase().capitalize()
            val id : Long? = ListDatabase.getInstance(context)?.listEntityDao()?.insert(listEntity)
            id
        }, onPostExecuteHandler = { result ->
            if (result != null && result < 0) {
                onFailure()
            } else {
                onSuccess()
            }
        })
    }

    fun deleteList(context: Context, listId: Long, onSuccess: () -> Unit) {
        doAsync(handler = {
            ListDatabase.getInstance(context)?.listEntityDao()?.deleteById(listId)
        }, onPostExecuteHandler = {
            onSuccess()
        })
    }

    fun searchByName(context: Context, q: String, supermarketId: String, onSuccess: (List<ListEntity>) -> Unit) {
        doAsync(handler = {
            val lists: List<ListEntity>? = ListDatabase.getInstance(context)?.listEntityDao()?.searchListByName(q, supermarketId)
            if (lists != null) {
                onSuccess(lists)
            }
        })
    }
}