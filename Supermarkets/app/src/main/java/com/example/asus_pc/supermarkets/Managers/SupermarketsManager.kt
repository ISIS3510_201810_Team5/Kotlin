package com.example.asus_pc.supermarkets.Managers

import android.location.Location
import com.example.asus_pc.supermarkets.Models.Supermarket
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

object SupermarketsManager {
    private val COLLECTION_NAME: String = "supermarkets"
    private val db: CollectionReference

    init {
        db = FirebaseFirestore.getInstance().collection(COLLECTION_NAME)
    }

    // API
    fun findCurrentSupermarket(lat: Double, lon: Double, onSuccess: (Supermarket) -> Unit, onFailure: () -> Unit) {
        val currentLocation = Location("")
        currentLocation.latitude = lat
        currentLocation.longitude = lon
        db.get()
                .addOnCompleteListener(OnCompleteListener { task ->
                    if (task.isSuccessful) {
                        val result = task.result
                        var foundSupermarket: Supermarket? = null
                        for (data in result.documents) {
                            val superMarketData = data.data
                            if (superMarketData != null) {
                                val superMarket = Supermarket(data.id, superMarketData)
                                val superMarketLocation = Location("")
                                superMarketLocation.latitude = superMarket.location.latitude
                                superMarketLocation.longitude = superMarket.location.longitude
                                if (currentLocation.distanceTo(superMarketLocation) <= 1000) {
                                    foundSupermarket = superMarket
                                    break
                                }
                            }
                        }
                        if(foundSupermarket != null) {
                            AnaliticsManager.registerSupermarketEntrance(foundSupermarket.id)
                            onSuccess(foundSupermarket)
                        } else {
                            onFailure()
                        }
                    } else {
                        onFailure()
                    }
                })
    }

    // Helpers
    fun getSurpermarketReference(supermarketId: String): DocumentReference {
        return db.document(supermarketId)
    }
}