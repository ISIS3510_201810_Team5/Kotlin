package com.example.asus_pc.supermarkets.Managers

import com.example.asus_pc.supermarkets.Models.Section
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore

object SectionsManager {
    private val COLLECTION_NAME: String = "sections"
    private val db: CollectionReference

    init {
        db = FirebaseFirestore.getInstance().collection(COLLECTION_NAME)
    }

    // API
    fun findSectionsFromSupermarket(q: String, supermarketId: String, onSuccess: (ArrayList<Section>) -> Unit, onFailure: () -> Unit) {
        val supermarket = SupermarketsManager.getSurpermarketReference(supermarketId)
        db.whereEqualTo("supermarket", supermarket)
                .limit(50)
                .orderBy("name")
                .startAt(q.toLowerCase())
                .endAt(q.toLowerCase() + "\uf8ff")
                .get()
                .addOnCompleteListener(OnCompleteListener { task ->
                    if (task.isSuccessful) {
                        val results = task.result
                        val sections = ArrayList<Section>()
                        for (result in results) {
                            val section: Map<String, Any> = result.data
                            sections.add(Section(result.id, section))
                        }
                        onSuccess(sections)
                    } else {
                        onFailure()
                    }
                })
    }

    // Helpers
    fun getSectionReference(sectiondId: String) : DocumentReference {
        return db.document(sectiondId)
    }
}