package com.example.asus_pc.supermarkets.Activities

import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.view.MenuItem
import android.view.View
import com.example.asus_pc.supermarkets.Fragments.BeMyEyesFragment
import com.example.asus_pc.supermarkets.Fragments.CallAssistanceFragment
import com.example.asus_pc.supermarkets.Fragments.HomeFragment
import com.example.asus_pc.supermarkets.Helpers.EcoSpeechRecognizer
import com.example.asus_pc.supermarkets.Interfaces.IEcoSpeechRecognizer
import com.example.asus_pc.supermarkets.R
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_route_be_my_eyes.*

class RouteBeMyEyesActivity : EcoActivity(), View.OnClickListener {

    val manager = supportFragmentManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_route_be_my_eyes)
        replaceFragment(BeMyEyesFragment())
        askAppPermissions({},{})
        btn_go_back.setOnClickListener(this)
        this.initializeTextToSpeech(this, {
            speak("Lee  productos con tu cámara", false)
        })
    }

    override fun onDestroy() {
        super.onDestroy()
        this.destroyTextToSpeech()
    }

    // Helpers
    fun replaceFragment(fragment: Fragment) {
        val transaction = manager.beginTransaction();
        transaction.replace(R.id.fragmet_holder, fragment);
        transaction.commit();
    }

    override fun onClick(view: View?) {
        when(view) {
            btn_go_back -> {
                finish()
            }
        }
    }
}