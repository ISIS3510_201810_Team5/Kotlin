package com.example.asus_pc.supermarkets.Activities

import android.os.Bundle
import android.view.View
import android.view.animation.Animation
import android.view.animation.RotateAnimation
import com.example.asus_pc.supermarkets.Helpers.Direction.DirectionStrategy
import com.example.asus_pc.supermarkets.Helpers.Direction.VectorDirectionProvider
import com.example.asus_pc.supermarkets.Helpers.EcoPreferences
import com.example.asus_pc.supermarkets.Helpers.RouteHandler
import com.example.asus_pc.supermarkets.Interfaces.IDirectionListener
import com.example.asus_pc.supermarkets.Interfaces.IRouteHandler
import com.example.asus_pc.supermarkets.Models.BeaconRoute
import com.example.asus_pc.supermarkets.R
import kotlinx.android.synthetic.main.activity_route.*

class RouteActivity : EcoActivity(), View.OnClickListener, IRouteHandler, IDirectionListener {

    var preferences: EcoPreferences? = null
    var lastReadTime = 0L
    var shouldUseDirection = false

    // Direction related
    lateinit var directionStrategy: DirectionStrategy
    lateinit var routeHandler: RouteHandler

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_route)

        btn_route_cancel.setOnClickListener(this)
        btn_add_new_stop.setOnClickListener(this)
        btn_call_assistance.setOnClickListener(this)
        error_image.setOnClickListener(this)

        // Route
        val stringDestinations = getExtraStringArray(SectionsActivity.DESTINATIONS_KEY)
        preferences = EcoPreferences(this)
        val supermarketId: String = preferences?.getSupermarketId()!!

        routeHandler = RouteHandler(this, stringDestinations, supermarketId)
        routeHandler.routeHandleListener = this

        // Direction
        shouldUseDirection = if(preferences?.getShouldUseDirection() == true) true else false

        directionStrategy = VectorDirectionProvider(this)
        directionStrategy.onValueChangedListener = this
        this.initializeTextToSpeech(this, {
            speak(txtRouteStatus.text.toString())
        })
    }

    override fun onStart() {
        super.onStart()
        routeHandler.start()
        startDirectionListener()
    }

    override fun onStop() {
        super.onStop()
        routeHandler.stop()
        stopDirectionListener()
    }

    override fun onDestroy() {
        super.onDestroy()
        routeHandler.destroy()
        stopDirectionListener()
        this.destroyTextToSpeech()
    }

    // Listeners
    override fun onClick(v: View?) {
        when (v) {
            btn_route_cancel -> {
                btn_route_cancel.isEnabled = false
                finish()
            }
            btn_add_new_stop -> {
                goToActivityWithExtraStringArray(this, SectionsActivity::class.java, true, SectionsActivity.ALREDY_SELECTED_DESTINATIONS_KEY, routeHandler.destinations.toTypedArray())
            }
            btn_call_assistance -> {
                goToActivity(this, RouteAssistanceActivity::class.java, true)
            }
            error_image -> {
                routeHandler.tryReconnect()
            }
        }
    }

    // Route Handler
    override fun onStartedRoute() {
        if (userHasAccelerometer(this) && userHasMagnetometer(this)) {
            startDirectionListener()
        }
    }

    override fun onFinishRoute(shouldFinishedRoute: Boolean) {
        stopDirectionListener()

        if (shouldFinishedRoute) {
            goToActivityWithExtraString(this, ArrivedActivity::class.java, true, ArrivedActivity.FINISH_ROUTE, ArrivedActivity.FINISH_ROUTE)
        } else {
            lastReadTime = 0L
            goToActivity(this, ArrivedActivity::class.java, false)
        }
    }

    override fun onChangedStatus(status: RouteHandler.STATUS) {
        setupStatus(status)
    }

    override fun onLostConnectivity(status: RouteHandler.STATUS) {
        stopDirectionListener()
        setupStatus(status)
    }

    override fun onApproachingDestination(distance: Double) {
        routeHandler.currentStep().distance = distance.toInt()
        setDirectionInstructions()
    }

    // Helpers
    fun setupCurrentRouteStep() {
        val routeStep: BeaconRoute = routeHandler.currentStep()
        if(!shouldUseDirection) {
            route_direction_image.setImageResource(routeStep.getImage())
            txtRouteStatus.setText(routeStep.toString())
            speak(txtRouteStatus.text.toString())
        }
        setDirectionInstructions()
    }

    fun setupStatus(status: RouteHandler.STATUS) {
        if (status == RouteHandler.STATUS.FINDING_POSITION) {
            changeVisibility(route_progress_bar, View.VISIBLE)
            changeVisibility(route_direction_image, View.GONE)
            changeVisibility(error_image, View.GONE)
            changeVisibility(btn_add_new_stop, View.GONE)
            changeVisibility(btn_call_assistance, View.GONE)
            val text = getString(R.string.route_activity_finding_position)
            txtRouteStatus.setText(text)
            speak(text)
        } else if (status == RouteHandler.STATUS.CALCULATING_ROUTE) {
            changeVisibility(route_progress_bar, View.VISIBLE)
            changeVisibility(route_direction_image, View.GONE)
            changeVisibility(error_image, View.GONE)
            changeVisibility(btn_add_new_stop, View.GONE)
            changeVisibility(btn_call_assistance, View.GONE)
            val text = getString(R.string.route_activity_calculating_route)
            txtRouteStatus.setText(text)
            speak(text)
        } else if (status == RouteHandler.STATUS.ON_ROUTE) {
            changeVisibility(route_progress_bar, View.GONE)
            changeVisibility(route_direction_image, View.VISIBLE)
            changeVisibility(error_image, View.GONE)
            changeVisibility(btn_add_new_stop, View.VISIBLE)
            changeVisibility(btn_call_assistance, View.GONE)
            setupCurrentRouteStep()
        } else if (status == RouteHandler.STATUS.NO_BLUETOOTH) {
            changeVisibility(route_progress_bar, View.GONE)
            changeVisibility(route_direction_image, View.GONE)
            error_image.setImageResource(R.drawable.ic_warning_light)
            changeVisibility(btn_add_new_stop, View.GONE)
            changeVisibility(error_image, View.VISIBLE)
            changeVisibility(btn_call_assistance, View.VISIBLE)
            val text = getString(R.string.error_no_bluetooth)
            txtRouteStatus.setText(text)
            speak(text)
        } else if (status == RouteHandler.STATUS.NO_CONNECTIVITY) {
            changeVisibility(route_progress_bar, View.GONE)
            changeVisibility(route_direction_image, View.GONE)
            error_image.setImageResource(R.drawable.ic_connectivity_light)
            changeVisibility(btn_add_new_stop, View.GONE)
            changeVisibility(error_image, View.VISIBLE)
            changeVisibility(btn_call_assistance, View.VISIBLE)
            val text = getString(R.string.error_no_connectivity)
            txtRouteStatus.setText(text)
            speak(text)
        } else if (status == RouteHandler.STATUS.NO_BLUETOOTH_CONNECTIVITY) {
            changeVisibility(route_progress_bar, View.GONE)
            changeVisibility(route_direction_image, View.GONE)
            error_image.setImageResource(R.drawable.ic_warning_light)
            changeVisibility(btn_add_new_stop, View.GONE)
            changeVisibility(error_image, View.VISIBLE)
            changeVisibility(btn_call_assistance, View.VISIBLE)
            val text = getString(R.string.error_no_bluetooth_connectivity)
            txtRouteStatus.setText(text)
            speak(text)
        } else if (status == RouteHandler.STATUS.HIGH_MAGNETIC_FIELD) {
            changeVisibility(route_progress_bar, View.GONE)
            changeVisibility(route_direction_image, View.GONE)
            error_image.setImageResource(R.drawable.ic_warning_light)
            changeVisibility(btn_add_new_stop, View.GONE)
            changeVisibility(error_image, View.VISIBLE)
            changeVisibility(btn_call_assistance, View.VISIBLE)
            val text = getString(R.string.error_high_magnetic_field)
            txtRouteStatus.setText(text)
            speak(text)
        } else if (status == RouteHandler.STATUS.NO_POSITION_FOUND) {
            changeVisibility(route_progress_bar, View.GONE)
            changeVisibility(route_direction_image, View.GONE)
            error_image.setImageResource(R.drawable.ic_warning_light)
            changeVisibility(btn_add_new_stop, View.GONE)
            changeVisibility(error_image, View.VISIBLE)
            changeVisibility(btn_call_assistance, View.VISIBLE)
            val text = getString(R.string.route_activity_no_position)
            txtRouteStatus.setText(text)
            speak(text)
        } else if (status == RouteHandler.STATUS.NO_ROUTE_FOUND) {
            changeVisibility(route_progress_bar, View.GONE)
            changeVisibility(route_direction_image, View.GONE)
            error_image.setImageResource(R.drawable.ic_warning_light)
            changeVisibility(btn_add_new_stop, View.GONE)
            changeVisibility(error_image, View.VISIBLE)
            changeVisibility(btn_call_assistance, View.VISIBLE)
            val text = getString(R.string.route_activity_no_route)
            txtRouteStatus.setText(text)
            speak(text)
        }
    }

    // Direction Handler
    override fun onRotationChanged(azimuth: Float, pitch: Float, oldDegree: Float) {
        if (routeHandler.isRouteFinished()) return
        val wantedDegree = routeHandler.currentStep().bearing

        val endDegree = wantedDegree - azimuth

        val ra = RotateAnimation(
                endDegree,
                endDegree,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF,
                0.5f)
        ra.duration = 300
        ra.fillAfter = true
        route_direction_image.startAnimation(ra)

        setDirectionInstructions(azimuth)
    }

    fun setDirectionInstructions(azimuth: Float? = null) {
        if (System.currentTimeMillis() - lastReadTime >= 5000) {
            val routeStep: BeaconRoute = routeHandler.currentStep()
            if(shouldUseDirection && azimuth != null) {
                val text = directionStrategy.getMoveDirections(azimuth, routeStep)
                txtRouteStatus.setText(text)
                route_direction_image.setImageResource(routeStep.getForwardImage())
                speak(txtRouteStatus.text.toString())
                lastReadTime = System.currentTimeMillis()
            } else if(!shouldUseDirection) {
                txtRouteStatus.setText(routeStep.toString())
                route_direction_image.setImageResource(routeStep.getImage())
                speak(txtRouteStatus.text.toString())
                lastReadTime = System.currentTimeMillis()
            }
        }
    }

    override fun onMagneticFieldChanged(value: Float) {
        routeHandler.lastMagneticField = value
    }

    fun stopDirectionListener() {
        this.directionStrategy.stop()
        route_direction_image.clearAnimation()
    }

    fun startDirectionListener() {
        if (shouldUseDirection == true) {
            directionStrategy.start()
        }
    }
}