package com.example.asus_pc.supermarkets.Managers

import android.content.ContentValues
import android.util.Log
import com.example.asus_pc.supermarkets.Models.Message
import com.example.asus_pc.supermarkets.Models.MessageQuery
import com.example.asus_pc.supermarkets.Models.Section
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.firestore.*


object MessageManager {
    private val COLLECTION_NAME: String = "messages"
    private val db: CollectionReference

    init {
        db = FirebaseFirestore.getInstance().collection(COLLECTION_NAME)
    }

    // API
    fun createMessage(supermarketId: String, sectionId: String, onSuccess: (String) -> Unit, onFailure: () -> Unit) {
        val message = MessageQuery()
        message.section = SectionsManager.getSectionReference(sectionId)
        db.add(message)
                .addOnCompleteListener(OnCompleteListener { task ->
                    if (task.isSuccessful) {
                        val id = task.result.id
                        onSuccess(id)
                    } else {
                        onFailure()
                    }
                })
    }

    fun findMessageById(messageId: String, onSuccess: (Message) -> Unit, onFailure: () -> Unit) {
        db.document(messageId)
                .get()
                .addOnCompleteListener(OnCompleteListener { task ->
                    if (task.isSuccessful) {
                        val result = task.result
                        val messageMap: Map<String, Any>? = result.data
                        if (messageMap != null) {
                            val message = Message(result.id, messageMap)
                            onSuccess(message)
                        } else {
                            onFailure()
                        }
                    } else {
                        onFailure()
                    }
                })
    }

    fun cancelMessageById(messageId: String, onSuccess: () -> Unit) {
        db.document(messageId)
                .update("status", 3)
                .addOnSuccessListener(OnSuccessListener {
                    onSuccess()
                })
    }

    fun listenToChanges(messageId: String, onMessageChange: (Message) -> Unit): ListenerRegistration {
        val docRef = db.document(messageId)
        return docRef.addSnapshotListener(EventListener { task, e ->
            if (e == null && task != null) {
                val result = task.data
                if (result != null) {
                    val message = Message(task.id, result)
                    onMessageChange(message)
                }
            }
        })
    }
}