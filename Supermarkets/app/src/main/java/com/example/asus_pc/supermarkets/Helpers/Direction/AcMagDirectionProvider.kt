package com.example.asus_pc.supermarkets.Helpers.Direction

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorManager

class AcMagDirectionProvider : DirectionStrategy {

    // Sensors
    val accelerometerSensor: Sensor
    val magneticSensor: Sensor

    // Data
    val orientation = FloatArray(3)
    val gravity = FloatArray(3)
    val geomagnetic = FloatArray(3)
    val R = FloatArray(9)
    val I = FloatArray(9)
    val intervalTime = 0
    var lastTime: Long = 0

    constructor(context: Context) : super(context) {
        accelerometerSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)
        magneticSensor = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD)
    }

    override  fun start() {
        sensorManager.registerListener(this, accelerometerSensor, SensorManager.SENSOR_DELAY_FASTEST)
        sensorManager.registerListener(this, magneticSensor, SensorManager.SENSOR_DELAY_FASTEST)
    }

    // Listeners
    override fun onSensorChanged(event: SensorEvent?) {
        if (onValueChangedListener == null || event == null) return
        val alpha: Float = 0.97f
        val time: Long = System.currentTimeMillis()
        if (time - lastTime > intervalTime) {
            synchronized(this, {
                if (event.sensor.type == Sensor.TYPE_ACCELEROMETER) {
                    gravity[0] = alpha * gravity[0] + (1 - alpha) * event.values[0]
                    gravity[1] = alpha * gravity[1] + (1 - alpha) * event.values[1]
                    gravity[2] = alpha * gravity[2] + (1 - alpha) * event.values[2]
                }

                if (event.sensor.type == Sensor.TYPE_MAGNETIC_FIELD) {
                    geomagnetic[0] = alpha * geomagnetic[0] + (1 - alpha) * event.values[0]
                    geomagnetic[1] = alpha * geomagnetic[1] + (1 - alpha) * event.values[1]
                    geomagnetic[2] = alpha * geomagnetic[2] + (1 - alpha) * event.values[2]


                    val magneticField = Math.sqrt((geomagnetic[0] * geomagnetic[0]
                            + geomagnetic[1] * geomagnetic[1]
                            + geomagnetic[2] * geomagnetic[2]).toDouble()).toFloat()

                    onValueChangedListener.onMagneticFieldChanged(magneticField)
                }

                val success: Boolean = SensorManager.getRotationMatrix(R, I, gravity, geomagnetic)
                if (success) {
                    SensorManager.getOrientation(R, orientation)
                    var azimuth: Float = Math.toDegrees(orientation[0].toDouble()).toFloat()
                    azimuth = (azimuth + 360) % 360
                    val pitch: Float = Math.toDegrees(orientation[1].toDouble()).toFloat()
                    val roll: Float = Math.toDegrees(orientation[2].toDouble()).toFloat()
                    onValueChangedListener.onRotationChanged(azimuth, pitch, roll)
                }
            })
            lastTime = time
        }
    }
}