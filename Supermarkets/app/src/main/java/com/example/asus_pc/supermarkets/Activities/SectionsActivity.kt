package com.example.asus_pc.supermarkets.Activities

import android.content.DialogInterface
import android.os.Bundle
import android.os.Handler
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import com.example.asus_pc.supermarkets.Adapters.SectionsAdapter
import com.example.asus_pc.supermarkets.Helpers.EcoSpeechRecognizer
import com.example.asus_pc.supermarkets.Interfaces.IEcoSpeechRecognizer
import com.example.asus_pc.supermarkets.R
import kotlinx.android.synthetic.main.activity_sections.*
import java.util.*
import android.view.View
import com.example.asus_pc.supermarkets.Entities.ListEntity
import com.example.asus_pc.supermarkets.Helpers.EcoPreferences
import com.example.asus_pc.supermarkets.Interfaces.ISectionsListener
import com.example.asus_pc.supermarkets.Managers.SectionsManager
import com.example.asus_pc.supermarkets.Models.Section
import com.example.asus_pc.supermarkets.Tasks.UIMainThread
import com.example.asus_pc.supermarkets.Fragments.ListFragment
import com.example.asus_pc.supermarkets.Managers.AnaliticsManager
import com.example.asus_pc.supermarkets.Managers.ListManager


class SectionsActivity : EcoActivity(), View.OnClickListener, TextWatcher, IEcoSpeechRecognizer, ISectionsListener {

    enum class MODE {
        CREATE_ROUTE,
        CREATE_LIST
    }

    companion object {
        val DESTINATIONS_KEY = "DESTINATIONS_KEY"
        val ALREDY_SELECTED_DESTINATIONS_KEY = "ALREDY_SELECTED_DESTINATIONS_KEY"
        val LIST_MODE = "LIST_MODE"
    }

    val DELAY: Long = 1000
    val sections = ArrayList<Section>()
    lateinit var adapter: SectionsAdapter
    var timer: Timer = Timer()
    var mode: MODE = MODE.CREATE_ROUTE

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_sections)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        txt_categories_search.addTextChangedListener(this)
        categories_recycler_view.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        btn_start_route.setOnClickListener(this)

        adapter = SectionsAdapter(sections, this)
        loadAlreadySelectedSections()
        categories_recycler_view.adapter = adapter
        loadCategories("")
        EcoSpeechRecognizer.subscribe(this)

        if (getExtraOptionalString(SectionsActivity.LIST_MODE) != null) {
            mode = MODE.CREATE_LIST
            btn_start_route.setText(getString(R.string.sections_activity_btn_create_list))
        }

        this.initializeTextToSpeech(this, {
            if(mode == MODE.CREATE_LIST) {
                speak("Vista de crear nueva lista")
            } else {
                speak("Vista de seleccionar secciones")
            }
        })
    }

    override fun onStart() {
        super.onStart()
        handleInternetConnection()
    }

    override fun onDestroy() {
        super.onDestroy()
        EcoSpeechRecognizer.unsubscribe(this)
        this.destroyTextToSpeech()
    }

    override fun onStop() {
        super.onStop()
        EcoSpeechRecognizer.unsubscribe(this)
    }

    override fun onPause() {
        super.onPause()
        EcoSpeechRecognizer.unsubscribe(this)
    }

    // Listeners
    fun loadCategories(q: String) {
        val preferences = EcoPreferences(this)
        val superMarketId = preferences.getSupermarketId()
        if (superMarketId != null) {
            UIMainThread.changeVisibility(this, categories_recycler_view, View.GONE)
            UIMainThread.changeVisibility(this, sections_progress_bar, View.VISIBLE)
            SectionsManager.findSectionsFromSupermarket(q, superMarketId, { response ->
                if (response.size == 0 && userHasInternet()) {
                    clearRecyclerView()
                    notFound.visibility = View.VISIBLE
                } else {
                    adapter.sectionsList = response
                    adapter.notifyDataSetChanged()
                    if (notFound.visibility == View.VISIBLE) notFound.visibility = View.GONE
                }
                handleInternetConnection()
            }, {
                handleInternetConnection()
            })
        }
    }

    override fun afterTextChanged(s: Editable) {
        if (s.length >= 3 || s.length == 0) {
            timer = Timer()
            timer.schedule(object : TimerTask() {
                override fun run() {
                    loadCategories(s.toString())
                }

            }, DELAY)
        }
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        timer?.cancel()
    }

    override fun onResult(text: String) {
        if (text.toLowerCase().equals("cereal") || text.toLowerCase().equals("lacteos") || text.toLowerCase().equals("entrada") || text.toLowerCase().equals("caja")) {
            txt_categories_search.setText(text.toLowerCase())
        } else if(text.toLowerCase().equals("borrar")) {
            txt_categories_search.setText("")
        } else if (text.toLowerCase().equals("elegir")) {
            Handler().postDelayed(Runnable { categories_recycler_view.findViewHolderForAdapterPosition(0).itemView.performClick() }, 1)
        } else if(text.toLowerCase().equals("empezar")) {
            btn_start_route.performClick()
        }
    }

    override fun onPartialResult(text: String) {
        println("EcoLogger: Se escucho \'" + text + "\'")
    }

    // Sections listener

    override fun onStartAvailable() {
        // Pedir permisos
        changeVisibility(btn_start_route, View.VISIBLE)
    }

    override fun onStartUnavailable() {
        changeVisibility(btn_start_route, View.GONE)
    }

    // Helpers
    fun loadAlreadySelectedSections() {
        val receivedSelectedSections = getExtraOptionalStringArray(ALREDY_SELECTED_DESTINATIONS_KEY)
        if (receivedSelectedSections != null && receivedSelectedSections.size > 0) {
            supportActionBar?.setDisplayHomeAsUpEnabled(false)
            val alreadySelected = ArrayList<Section>()
            lateinit var section: Section
            for (selectedSection in receivedSelectedSections) {
                section = Section("", "", selectedSection, "")
                alreadySelected.add(section)
            }
            adapter.selection = alreadySelected
            adapter.notifyDataSetChanged()
            onStartAvailable()
        }
    }

    fun registerSearches(search_sections: ArrayList<Section>) {
        for(section in search_sections) {
            AnaliticsManager.registerSectionSearched(section.id)
        }
    }

    fun handleInternetConnection() {
        UIMainThread.changeVisibility(this, categories_recycler_view, View.VISIBLE)
        UIMainThread.changeVisibility(this, sections_progress_bar, View.GONE)
        if (!userHasInternet()) {
            clearRecyclerView()
            mistake.visibility = View.VISIBLE
            TryAgain.visibility = View.VISIBLE
            TryAgain.setOnClickListener() {
                handleInternetConnection()
            }
        } else {
            if (mistake.visibility == View.VISIBLE) {
                mistake.visibility = View.GONE
                TryAgain.visibility = View.GONE
                val search_text = txt_categories_search.text.trim()
                loadCategories(search_text.toString())
            }

        }
    }

    fun clearRecyclerView() {
        this.adapter.sectionsList.clear()
        this.adapter.notifyDataSetChanged()
    }

    fun destinationsArrayToStringArray(destinations: ArrayList<Section>): Array<String> {
        val route: ArrayList<String> = ArrayList<String>()
        for (section in destinations) {
            route.add(section.beaconUuid)
        }
        return route.toTypedArray()
    }

    override fun onClick(v: View?) {
        when (v) {
            btn_start_route -> {
                val selected_sections = adapter.selection
                val destinations = destinationsArrayToStringArray(selected_sections)
                if (mode == MODE.CREATE_LIST) {
                    this.showInputDialogWithOneButton("Alerta", "¿Cuál es el nombre de la lista?", InputType.TYPE_CLASS_TEXT, "Ok", { name ->
                        val preferences = EcoPreferences(this)
                        val superMarketId = preferences.getSupermarketId()
                        if (superMarketId != null) {
                            val listEntity: ListEntity = ListEntity(null, name, superMarketId, destinations)
                            ListManager.addList(this.applicationContext, listEntity, {
                                finishActivityWithResult(ListFragment.CREATED_LIST_RESULT_CODE)
                            }, {
                                showDialogWithOneButton(getString(R.string.error_dialog_title), getString(R.string.error_dialog_list_already_exists), getString(R.string.error_dialog_button), DialogInterface.OnClickListener { dialog, which -> })
                            })
                        } else {
                            finish()
                        }
                    })
                } else {
                    registerSearches(selected_sections)
                    goToActivityWithExtraStringArray(this, RouteActivity::class.java, true, SectionsActivity.DESTINATIONS_KEY, destinations)
                }
            }
        }
    }

    override fun onBackPressed() {
        if (intent.hasExtra(ALREDY_SELECTED_DESTINATIONS_KEY)) {
            // Do nothing
        } else {
            super.onBackPressed()
        }
    }
}
