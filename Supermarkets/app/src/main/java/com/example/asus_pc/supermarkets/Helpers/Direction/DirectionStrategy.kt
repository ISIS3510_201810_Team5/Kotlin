package com.example.asus_pc.supermarkets.Helpers.Direction

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import com.example.asus_pc.supermarkets.Interfaces.IDirectionListener
import com.example.asus_pc.supermarkets.Models.BeaconRoute
import kotlin.math.absoluteValue
import kotlin.math.roundToInt

abstract class DirectionStrategy : SensorEventListener {

    // Managers
    val sensorManager: SensorManager

    // Listener
    lateinit var onValueChangedListener: IDirectionListener

    constructor(context: Context) {
        sensorManager = context.getSystemService(Context.SENSOR_SERVICE) as SensorManager
    }

    // Methods
    abstract fun start()

    fun stop() {
        sensorManager.unregisterListener(this)
    }

    fun getDirectionText(degree: Float): String {
        val step = 22.5f
        if (degree >= 0 && degree < step || degree > 360 - step) {
            return "N"
        }
        if (degree >= step && degree < step * 3) {
            return "NE"
        }
        if (degree >= step * 3 && degree < step * 5) {
            return "E"
        }
        if (degree >= step * 5 && degree < step * 7) {
            return "SE"
        }
        if (degree >= step * 7 && degree < step * 9) {
            return "S"
        }
        if (degree >= step * 9 && degree < step * 11) {
            return "SW"
        }
        if (degree >= step * 11 && degree < step * 13) {
            return "W"
        }
        return if (degree >= step * 13 && degree < step * 15) {
            "NW"
        } else ""
    }

    fun getMoveDirections(currentDegree: Float, beaconRoute: BeaconRoute) : String {
        val wantedDegree = beaconRoute.bearing.toFloat()
        val currentDirectionText = getDirectionText(currentDegree)
        val wantedDirectionText = getDirectionText(wantedDegree)
        if(currentDirectionText.equals(wantedDirectionText, ignoreCase = true)) return "Camina hacia adelante, ${beaconRoute.distance} ${beaconRoute.unit}"
        else {
            val difference = wantedDegree - currentDegree
            val direccion = if(difference < 0) "izquierda" else "derecha"
            return "Gire " + difference.absoluteValue.roundToInt() + " grados hacia la " + direccion
        }
    }

    // Listeners
    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {
    }
}