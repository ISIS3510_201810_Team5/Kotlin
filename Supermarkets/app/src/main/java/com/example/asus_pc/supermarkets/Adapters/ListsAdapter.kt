package com.example.asus_pc.supermarkets.Adapters

import android.content.DialogInterface
import android.support.v7.widget.RecyclerView
import android.view.HapticFeedbackConstants
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.asus_pc.supermarkets.Entities.ListEntity
import com.example.asus_pc.supermarkets.Interfaces.IListItemListener
import com.example.asus_pc.supermarkets.Managers.ListManager
import com.example.asus_pc.supermarkets.R
import kotlinx.android.synthetic.main.item_list_lists.view.*

class ListsAdapter(var lists: List<ListEntity>, val listItemListener: IListItemListener) : RecyclerView.Adapter<ListsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_list_lists, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return lists.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val currentList = lists.get(position)

        // Set views
        holder.itemView.item_list_lists_title.setText(currentList.name)

        holder.itemView.setOnClickListener { view ->
            listItemListener.OnClickListener(currentList)
        }

        holder.itemView.setOnLongClickListener { view ->
            // Vibrar para notificar que se dio el long press
            view.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS)
            listItemListener.OnLongClickListener(currentList)
            true
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    }

    // Helpers
}