package com.example.asus_pc.supermarkets.Helpers;

// Taken from https://stackoverflow.com/a/42973475/5245639
public class LowPassFilter {
    // Time constant in seconds
    private static final float timeConstant = 0.297f;
    private float alpha = 0.15f;
    private float timestampOld = System.nanoTime();
    private float output[] = new float[]{0, 0, 0};

    private long count = 0;

    public float[] lowPass(float[] input) {
        float timestamp = System.nanoTime();

        // Find the sample period (between updates).
        // Convert from nanoseconds to seconds
        float dt = 1 / (count / ((timestamp - timestampOld) / 1000000000.0f));

        count++;

        // Calculate alpha
        alpha = timeConstant / (timeConstant + dt);

        output[0] = calculate(input[0], output[0]);
        output[1] = calculate(input[1], output[1]);
        output[2] = calculate(input[2], output[2]);

        return output;
    }


    private float calculate(float input, float output) {
        return alpha * output + (1 - alpha) * input;
    }
}