package com.example.asus_pc.supermarkets.Interfaces

import com.example.asus_pc.supermarkets.Entities.ListEntity

interface IListItemListener {

    fun OnClickListener(listItem: ListEntity)

    fun OnLongClickListener(listItem: ListEntity)
}