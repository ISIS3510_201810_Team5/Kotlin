package com.example.asus_pc.supermarkets.Models.Analitics

import com.example.asus_pc.supermarkets.Models.Message
import com.google.firebase.firestore.DocumentReference
import java.util.*

class SupermarketSearchQuery {
    var supermarket : DocumentReference? = null
    var createdAt: Date = Date()
}