package com.example.asus_pc.supermarkets.Tasks

import android.os.AsyncTask

class doAsync(val handler: () -> Unit = {}, val onPostExecuteHandler: () -> Unit = {}) : AsyncTask<Void, Void, Void>() {
    init {
        execute()
    }

    override fun doInBackground(vararg params: Void?): Void? {
        handler()
        return null
    }

    override fun onPostExecute(result: Void?) {
        super.onPostExecute(result)
        onPostExecuteHandler()
    }
}