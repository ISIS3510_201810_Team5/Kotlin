package com.example.asus_pc.supermarkets.Models.Analitics

import com.google.firebase.firestore.DocumentReference
import java.util.*

class SectionSearchQuery {
    var section : DocumentReference? = null
    var createdAt: Date = Date()
}