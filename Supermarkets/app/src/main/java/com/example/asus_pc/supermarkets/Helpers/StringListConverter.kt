package com.example.asus_pc.supermarkets.Helpers

import android.arch.persistence.room.TypeConverter

class StringListConverter {

    @TypeConverter
    fun fromStringToList(value: String): Array<String> {
        return value.split(";").toTypedArray()
    }

    @TypeConverter
    fun fromListToString(list: Array<String>): String {
        var string = ""
        return list.joinToString(";","","",list.size,"")
    }
}