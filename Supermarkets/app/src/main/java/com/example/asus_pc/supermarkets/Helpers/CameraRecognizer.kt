package com.example.asus_pc.supermarkets.Helpers

import android.util.SparseArray
import android.view.SurfaceHolder
import android.view.SurfaceView
import com.example.asus_pc.supermarkets.Activities.EcoActivity
import com.example.asus_pc.supermarkets.Interfaces.ICameraRecognizer
import com.google.android.gms.vision.CameraSource
import com.google.android.gms.vision.Detector
import com.google.android.gms.vision.text.TextBlock
import com.google.android.gms.vision.text.TextRecognizer

class CameraRecognizer<Type> : SurfaceHolder.Callback, Detector.Processor<TextBlock> {

    val REQUESTED_FPS = 2.0f

    lateinit var cameraSource: CameraSource
    var dictionary: ArrayList<Type> = ArrayList<Type>()
    val cameraSurfaceView: SurfaceView
    val ecoActivity: EcoActivity
    val implementer: ICameraRecognizer<Type>
    val evaluator: (item: String, dictionary: ArrayList<Type>) -> Type?

    constructor(ecoActivity: EcoActivity, implementer: ICameraRecognizer<Type>, cameraSurfaceView: SurfaceView, evaluator: (item: String, dictionary: ArrayList<Type>) -> Type?) {
        this.ecoActivity = ecoActivity
        this.implementer = implementer
        this.cameraSurfaceView = cameraSurfaceView
        this.evaluator = evaluator
    }

    fun setupCameraSource() {
        this.dictionary = dictionary
        val textRecognizer: TextRecognizer = TextRecognizer.Builder(ecoActivity.applicationContext).build()

        if (!textRecognizer.isOperational) {
            implementer.onErrorSetingUp()
            println("EcoLogger: No ha acabado de hacer el setup del CameraRecognizer")
        } else {
            cameraSource = CameraSource.Builder(ecoActivity.applicationContext, textRecognizer)
                    .setFacing(CameraSource.CAMERA_FACING_BACK)
                    .setRequestedPreviewSize(1280, 1024)
                    .setAutoFocusEnabled(true)
                    .setRequestedFps(REQUESTED_FPS)
                    .build()

            cameraSurfaceView.holder.addCallback(this)
            textRecognizer.setProcessor(this)
        }
    }

    // Surface Holder interface
    override fun surfaceChanged(holder: SurfaceHolder?, format: Int, width: Int, height: Int) {
    }

    override fun surfaceDestroyed(holder: SurfaceHolder?) {
        cameraSource.stop()
    }

    override fun surfaceCreated(holder: SurfaceHolder?) {
        try {
            cameraSource.start(cameraSurfaceView.holder)
        } catch (e: SecurityException) {
            implementer.onErrorSetingUp()
            println("EcoLogger: Error al iniciar el camera source")
        }
    }

    // Detector processor interface
    override fun release() {
    }

    override fun receiveDetections(detections: Detector.Detections<TextBlock>?) {
        if (detections != null) {
            val items: SparseArray<TextBlock> = detections.detectedItems
            if (items.size() > 0) {
                var text: String = ""
                var item: String = ""
                var familiarItem : Type? = null
                for (i in 0 until items.size()) {
                    item = items.valueAt(i).value.toLowerCase()
                    text += "${item}\n"
                    familiarItem = evaluator(item, dictionary)
                    if (familiarItem != null) {
                        implementer.recognizedItem(familiarItem)
                        return
                    }
                }
            }
        }
    }
}