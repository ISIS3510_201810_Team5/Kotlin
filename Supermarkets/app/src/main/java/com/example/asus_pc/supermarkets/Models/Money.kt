package com.example.asus_pc.supermarkets.Models

class Money {

    val units : Long
    val iso : String

    constructor(units: Long, iso: String) {
        this.units = units
        this.iso = iso
    }

    constructor(data: Map<String, Any>) {
        this.units = data.get("units") as Long
        this.iso = data.get("iso") as String
    }
}