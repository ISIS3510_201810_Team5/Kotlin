package com.example.asus_pc.supermarkets.DAOS

import android.arch.persistence.room.*
import com.example.asus_pc.supermarkets.Entities.ListEntity

@Dao
interface ListEntityDAO {

    @Query("SELECT * from listData WHERE supermarket_id = :supermarketId")
    fun getAll(supermarketId: String): List<ListEntity>

    @Insert(onConflict = OnConflictStrategy.IGNORE )
    fun insert(listEntity: ListEntity) : Long

    @Query("DELETE FROM listData WHERE id = :id")
    fun deleteById(id: Long)

    @Query("SELECT * FROM listData WHERE name LIKE :name AND supermarket_id = :supermarketId")
    fun searchListByName(name: String, supermarketId: String): List<ListEntity>
}