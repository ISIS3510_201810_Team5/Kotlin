package com.example.asus_pc.supermarkets.Managers

import com.example.asus_pc.supermarkets.Models.Analitics.ProductSearchQuery
import com.example.asus_pc.supermarkets.Models.Analitics.SectionSearchQuery
import com.example.asus_pc.supermarkets.Models.Analitics.SupermarketSearchQuery
import com.google.firebase.firestore.FirebaseFirestore
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

object AnaliticsManager {
    private val SUPERMARKET_CHECKINS_COLLECTION: String = "supermarket_checkins"
    private val PRODUCT_SEARCH_COLLECTION: String = "product_search"
    private val SECTION_SEARCH_COLLECTION: String = "section_search"

    private val executorService: ExecutorService

    init {
        executorService = Executors.newCachedThreadPool()
    }

    // API
    fun registerSupermarketEntrance(supermarketId: String) {
        executorService.execute {
            val db = FirebaseFirestore.getInstance().collection(SUPERMARKET_CHECKINS_COLLECTION)
            val supermarket = SupermarketsManager.getSurpermarketReference(supermarketId)
            val supermarketSearchQuery: SupermarketSearchQuery = SupermarketSearchQuery()
            supermarketSearchQuery.supermarket = supermarket
            db.add(supermarketSearchQuery)
        }
    }

    fun registerProductSearched(productId: String) {
        executorService.execute {
            val db = FirebaseFirestore.getInstance().collection(PRODUCT_SEARCH_COLLECTION)
            val product = ProductsManager.getProductReference(productId)
            val productSearchQuery: ProductSearchQuery = ProductSearchQuery()
            productSearchQuery.product = product
            db.add(productSearchQuery)
        }
    }

    fun registerSectionSearched(sectionId: String) {
        executorService.execute {
            val db = FirebaseFirestore.getInstance().collection(SECTION_SEARCH_COLLECTION)
            val section = SectionsManager.getSectionReference(sectionId)
            val sectionSearchQuery: SectionSearchQuery = SectionSearchQuery()
            sectionSearchQuery.section = section
            db.add(sectionSearchQuery)
        }
    }
}