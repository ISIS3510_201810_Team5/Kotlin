package com.example.asus_pc.supermarkets.Helpers

import android.content.Context
import android.media.MediaPlayer
import com.example.asus_pc.supermarkets.Interfaces.IEcoSpeechRecognizer
import com.example.asus_pc.supermarkets.Tasks.EcoSpeechRecognizerAsync
import java.io.File
import java.io.IOException
import edu.cmu.pocketsphinx.*

object EcoSpeechRecognizer : RecognitionListener {

    val KWS_SEARCH = "wakeup"
    val MENU_SEARCH = "mymenu"
    val KEYPHRASE = "hola eco iniciar"

    lateinit var recognizer: SpeechRecognizer
    var sound: MediaPlayer? = null
    private val observers = ArrayList<IEcoSpeechRecognizer>()

    // Setup
    fun runRecognizerSetup(context: Context) {
        EcoSpeechRecognizerAsync(context.applicationContext).execute()
    }

    @Throws(IOException::class)
    fun setupRecognizer(assetsDir: File, sound: MediaPlayer) {
        recognizer = SpeechRecognizerSetup.defaultSetup()
                .setAcousticModel(File(assetsDir, "es-es-ptm"))
                .setDictionary(File(assetsDir, "es.dict"))
                .recognizer

        recognizer.addListener(this)
        recognizer.addKeyphraseSearch(KWS_SEARCH, KEYPHRASE)
        val menuGrammar = File(assetsDir, "mymenu.gram")
        recognizer.addGrammarSearch(MENU_SEARCH, menuGrammar)
        this.sound = sound
    }

    fun switchSearch(searchName: String) {
        recognizer.stop()
        if (searchName == KWS_SEARCH)
            recognizer.startListening(searchName)
        else {
            recognizer.startListening(searchName, 10000)
            sound?.start()
        }
    }

    // Listeners
    override fun onResult(hypothesis: Hypothesis?) {
        if (hypothesis != null) {
            val text = hypothesis.hypstr
            for (observer in observers) {
                observer.onResult(text)
            }
        }
    }

    override fun onPartialResult(hypothesis: Hypothesis?) {
        if (hypothesis == null)
            return

        val text = hypothesis.hypstr
        if (text.equals(KEYPHRASE))
            switchSearch(MENU_SEARCH)
        else {
            for (observer in observers) {
                observer.onPartialResult(text)
            }
        }
    }

    override fun onTimeout() {
        switchSearch(KWS_SEARCH);
    }

    override fun onBeginningOfSpeech() {
    }

    override fun onEndOfSpeech() {
        if (!recognizer?.getSearchName().equals(KWS_SEARCH))
            switchSearch(KWS_SEARCH);
    }

    override fun onError(error: Exception) {
        println(error.message)
    }

    fun destroy() {
        if (recognizer != null) {
            recognizer.cancel()
            recognizer.stop()
        }
    }

    // Observator pattern
    fun subscribe(observer: IEcoSpeechRecognizer) {
        observers.add(observer)
    }

    fun unsubscribe(observer: IEcoSpeechRecognizer) {
        observers.remove(observer)
    }
}