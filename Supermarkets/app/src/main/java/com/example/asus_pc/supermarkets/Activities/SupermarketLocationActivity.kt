package com.example.asus_pc.supermarkets.Activities

import android.annotation.SuppressLint
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.os.Handler
import android.view.View
import com.example.asus_pc.supermarkets.Helpers.EcoPreferences
import com.example.asus_pc.supermarkets.Helpers.Location.ProviderLocationTracker
import com.example.asus_pc.supermarkets.Interfaces.LocationTracker
import com.example.asus_pc.supermarkets.Managers.SupermarketsManager
import com.example.asus_pc.supermarkets.R
import kotlinx.android.synthetic.main.activity_supermarket_location.*
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices


class SupermarketLocationActivity : EcoActivity(), View.OnClickListener, LocationTracker.LocationUpdateListener {

    enum class STATUS {
        FINDING_POSITION,
        GPS_FOUND,
        NO_POSITION_FOUND,
        NO_CONNECTIVITY,
        NO_LOCATION,
        NO_CONNECTIVITY_LOCATION
    }

    var locationManager: LocationManager? = null
    var preferences: EcoPreferences? = null
    var status: STATUS = STATUS.FINDING_POSITION

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_supermarket_location)
        btn_calculate_location.setOnClickListener(this)
        preferences = EcoPreferences(this)

        if (hasConnectivity()) {
            findCurrentSupermarket()
        }
    }

    override fun onStart() {
        super.onStart()
    }

    // Listeners
    override fun onClick(view: View?) {
        when (view) {
            btn_calculate_location -> {
                if (hasConnectivity()) {
                    status = STATUS.FINDING_POSITION
                    setupStatus()
                    findCurrentSupermarket()
                }
            }
        }
    }

    override fun onUpdate(oldLoc: Location?, oldTime: Long, newLoc: Location?, newTime: Long) {
        if (newLoc != null) {
            status = STATUS.GPS_FOUND
            handleLocationFound(newLoc)
        }
    }

    // Helpers
    @SuppressLint("MissingPermission")
    fun findCurrentSupermarket() {
        btn_calculate_location.isEnabled = false
        locationManager = getSystemService(LOCATION_SERVICE) as LocationManager?
        askAppPermissions({
            val locationHelper = ProviderLocationTracker(this, ProviderLocationTracker.ProviderType.GPS)
            var lastKnown = locationManager?.getLastKnownLocation(LocationManager.GPS_PROVIDER)

            if (locationHelper.location != null) {
                status = STATUS.GPS_FOUND
                handleLocationFound(locationHelper.location)
            } else if (lastKnown != null) {
                status = STATUS.GPS_FOUND
                handleLocationFound(lastKnown)
            } else {
                val client = LocationServices.getFusedLocationProviderClient(this)
                val locationRequest = LocationRequest()
                LocationRequest.create();
                locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
                locationRequest.setMaxWaitTime(5000);
                locationRequest.setInterval(10000); // Update location every second
                client.lastLocation.addOnSuccessListener { result ->
                    if(result != null) {
                        handleLocationFound(result)
                    } else {
                        lastLocationResort(locationHelper)
                    }
                }.addOnFailureListener {
                    lastLocationResort(locationHelper)
                }
            }
        }, {
            status = STATUS.NO_POSITION_FOUND
            btn_calculate_location.isEnabled = true
            setupStatus()
        })
    }

    fun setupStatus() {
        if (status == STATUS.FINDING_POSITION) {
            changeVisibility(location_image, View.GONE)
            changeVisibility(location_progress_bar, View.VISIBLE)
            txt_location.setText(getString(R.string.supermarket_location_activity_finding_supermarket))
        } else if (status == STATUS.NO_POSITION_FOUND) {
            changeVisibility(location_image, View.VISIBLE)
            changeVisibility(location_progress_bar, View.GONE)
            location_image.setImageResource(R.drawable.ic_warning_light)
            txt_location.setText(getString(R.string.supermarket_location_activity_no_supermarket))
        } else if (status == STATUS.NO_CONNECTIVITY) {
            changeVisibility(location_image, View.VISIBLE)
            changeVisibility(location_progress_bar, View.GONE)
            location_image.setImageResource(R.drawable.ic_connectivity_light)
            txt_location.setText(getString(R.string.error_no_connectivity))
        } else if (status == STATUS.NO_LOCATION) {
            changeVisibility(location_image, View.VISIBLE)
            changeVisibility(location_progress_bar, View.GONE)
            location_image.setImageResource(R.drawable.ic_location_off_light)
            txt_location.setText(getString(R.string.error_no_location))
        } else if (status == STATUS.NO_CONNECTIVITY_LOCATION) {
            changeVisibility(location_image, View.VISIBLE)
            changeVisibility(location_progress_bar, View.GONE)
            location_image.setImageResource(R.drawable.ic_warning_light)
            txt_location.setText(getString(R.string.error_no_location_connectivity))
        }
    }

    fun handleLocationFound(location: Location) {
        val lat = location.latitude
        val lon = location.longitude
        SupermarketsManager.findCurrentSupermarket(lat, lon, { result ->
            preferences?.saveSupermarketId(result.id)
            preferences?.saveSupermarketPhone(result.phone)
            preferences?.saveSupermarketName(result.name)
            preferences?.saveShouldUseDirection(result.shouldUseDirection)
            if (preferences?.getSupermarketId() != null && preferences?.getSupermarketPhone() != null) {
                goToActivity(this, MainActivity::class.java, true)
            }
        }, {
            status = STATUS.NO_POSITION_FOUND
            btn_calculate_location.isEnabled = true
            setupStatus()
        })
    }

    fun hasConnectivity(): Boolean {
        var isConnected: Boolean = true
        if (!userHasInternet() && !userHasLocation()) {
            isConnected = false
            status = STATUS.NO_CONNECTIVITY_LOCATION
            setupStatus()
        } else if (!userHasInternet()) {
            isConnected = false
            status = STATUS.NO_CONNECTIVITY
            setupStatus()
        } else if (!userHasLocation()) {
            isConnected = false
            status = STATUS.NO_LOCATION
            setupStatus()
        }
        return isConnected
    }

    fun lastLocationResort(locationHelper: ProviderLocationTracker) {
        locationHelper.start(this)
        Handler().postDelayed({
            if (status != STATUS.GPS_FOUND && hasConnectivity()) {
                status = STATUS.NO_POSITION_FOUND
                btn_calculate_location.isEnabled = true
                setupStatus()
            }
        }, 20000)
    }
}
