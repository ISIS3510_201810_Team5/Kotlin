package com.example.asus_pc.supermarkets.Entities

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.Index
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "listData", indices = arrayOf(Index(value = ["name"], unique = true)))
data class ListEntity(
        @PrimaryKey(autoGenerate = true) var id: Long?,
        @ColumnInfo(name= "name") var name: String,
        @ColumnInfo(name= "supermarket_id") var supermarketId: String,
        @ColumnInfo(name= "destinations") var destinations: Array<String>
        ) {
    constructor() : this(null, "", "", Array(0) { "" })
}
