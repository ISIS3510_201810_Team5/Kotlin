package com.example.asus_pc.supermarkets.Helpers

import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import com.example.asus_pc.supermarkets.Activities.EcoActivity
import com.example.asus_pc.supermarkets.Activities.RouteActivity
import com.example.asus_pc.supermarkets.Activities.SectionsActivity
import com.example.asus_pc.supermarkets.Helpers.Direction.DirectionStrategy
import com.example.asus_pc.supermarkets.Helpers.Direction.VectorDirectionProvider
import com.example.asus_pc.supermarkets.Interfaces.IBeaconLocator
import com.example.asus_pc.supermarkets.Interfaces.IDirectionListener
import com.example.asus_pc.supermarkets.Interfaces.IRouteHandler
import com.example.asus_pc.supermarkets.Managers.BeaconManager
import com.example.asus_pc.supermarkets.Models.BeaconRoute
import com.example.asus_pc.supermarkets.Models.CalculatePositionQuery
import com.example.asus_pc.supermarkets.Models.CategoryBeacon
import com.example.asus_pc.supermarkets.Models.MultipleRoutingTableQuery
import org.altbeacon.beacon.Beacon
import org.altbeacon.beacon.Region
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class RouteHandler : IBeaconLocator {

    enum class STATUS {
        FINDING_POSITION,
        CALCULATING_ROUTE,
        ON_ROUTE,
        NO_BLUETOOTH,
        NO_CONNECTIVITY,
        HIGH_MAGNETIC_FIELD,
        NO_POSITION_FOUND,
        NO_ROUTE_FOUND,
        NO_BLUETOOTH_CONNECTIVITY
    }

    // Routing related
    var status: STATUS = STATUS.FINDING_POSITION
    var beaconLocator: BeaconLocator
    var route: Queue<Queue<BeaconRoute>>
    lateinit var origin: String
    var destinations: Queue<String>
    var lastMagneticField: Float = 0f
    var activity: EcoActivity
    var superMarketId: String

    // Listeners
    var routeHandleListener: IRouteHandler? = null


    constructor(activity: EcoActivity, stringDestinations: Array<String>, supermarketId: String) {
        this.activity = activity

        this.destinations = LinkedList<String>()
        this.route = LinkedList<Queue<BeaconRoute>>()
        this.superMarketId = supermarketId

        // Build destination queue
        for (destination in stringDestinations) {
            destinations.add(destination)
        }

        this.beaconLocator = BeaconLocator(this)
    }

    // Lifecycle
    fun start() {
        this.beaconLocator.startDetectingBeacons()
    }

    fun stop() {
        this.beaconLocator.stopDetectingBeacons()
    }

    fun destroy() {
        this.beaconLocator.destroy()
    }

    // Routing
    fun calculateCurrentPosition(beacons: Collection<Beacon>) {
        this.beaconLocator.stopDetectingBeacons()
        // Geat near beacons
        val categoryBeacons: ArrayList<String> = ArrayList<String>()
        var newBeacon: CategoryBeacon? = null
        for (b in beacons) {
            newBeacon = CategoryBeacon(b)
            if (newBeacon.distance <= 1.0) {
                categoryBeacons.add(newBeacon.uuid)
            }
        }

        val uuids: Array<String> = categoryBeacons.toTypedArray()
        val query: CalculatePositionQuery = CalculatePositionQuery(superMarketId, uuids)
        BeaconManager.calculateCurrentPosition(query, object : Callback<CategoryBeacon> {
            override fun onResponse(call: Call<CategoryBeacon>?, response: Response<CategoryBeacon>?) {
                if (response != null && response.isSuccessful) {
                    println("EcoLogger: Se encontro mi posicion")
                    status = RouteHandler.STATUS.CALCULATING_ROUTE
                    routeHandleListener?.onChangedStatus(status)
                    origin = response.body().uuid

                    if (origin.equals(destinations.peek(), ignoreCase = true)) {
                        if (destinations.size == 1) {
                            finishRoute()
                        } else if (destinations.size > 1) {
                            status = RouteHandler.STATUS.FINDING_POSITION
                            destinations.poll()
                            finishRoute(false)
                        }
                        // Else no deberia pasar nunca porque el destino no puede ser vacio o negativo
                    } else {
                        updateRouting()
                    }
                } else {
                    // Mostrar error
                    status = RouteHandler.STATUS.NO_POSITION_FOUND
                    routeHandleListener?.onChangedStatus(status)
                }
            }

            override fun onFailure(call: Call<CategoryBeacon>?, t: Throwable?) {
                println("EcoLogger: Hubo un error encontrando mi posicion")
                status = RouteHandler.STATUS.NO_POSITION_FOUND
                routeHandleListener?.onChangedStatus(status)
            }
        })

    }

    fun handleRouting(beacons: Collection<Beacon>) {
        var beacon: CategoryBeacon? = null
        val next: BeaconRoute = route.peek().peek()
        for (b in beacons) {
            beacon = CategoryBeacon(b)
            if (next.to.equals(beacon.uuid, ignoreCase = true)) {
                if (beacon.distance <= 1.0) {
                    route.peek().poll()

                    if (route.peek().isEmpty()) {
                        route.poll()

                        if (route.isEmpty()) {
                            finishRoute()
                        } else {
                            routeHandleListener?.onChangedStatus(status)
                            finishRoute(false)
                        }
                    } else {
                        routeHandleListener?.onChangedStatus(status)
                    }
                } else {
                    routeHandleListener?.onApproachingDestination(beacon.distance)
                }
            }
        }
    }

    fun updateRouting() {
        val query: MultipleRoutingTableQuery = MultipleRoutingTableQuery(superMarketId, origin, destinations.toTypedArray())
        BeaconManager.getMultipleRoutingTable(query, object : Callback<Array<Array<BeaconRoute>>> {
            override fun onResponse(call: Call<Array<Array<BeaconRoute>>>?, response: Response<Array<Array<BeaconRoute>>>?) {
                if (response != null && response.isSuccessful) {
                    val calculatedRoutes = BeaconManager.arrayToRouteOfRoutes(response.body())
                    route.clear()
                    route = calculatedRoutes
                    status = RouteHandler.STATUS.ON_ROUTE
                    routeHandleListener?.onStartedRoute()
                    beaconLocator.startDetectingBeacons()
                } else {
                    status = RouteHandler.STATUS.NO_ROUTE_FOUND
                }
                routeHandleListener?.onChangedStatus(status)
            }

            override fun onFailure(call: Call<Array<Array<BeaconRoute>>>?, t: Throwable?) {
                println("EcoLogger: Hubo un error encontrando una ruta al destino")
                status = RouteHandler.STATUS.NO_ROUTE_FOUND
                routeHandleListener?.onChangedStatus(status)
            }
        })

    }

    fun finishRoute(shouldFinishRoute: Boolean = true) {
        beaconLocator.stopDetectingBeacons()
        routeHandleListener?.onFinishRoute(shouldFinishRoute)
    }

    // Beacon Listeners
    override fun didRangeBeaconsInRegion(beacons: Collection<Beacon>, region: Region) {
        hasConnectivity()
        if (this.status == RouteHandler.STATUS.FINDING_POSITION) {
            calculateCurrentPosition(beacons)
        } else if (this.status == RouteHandler.STATUS.ON_ROUTE) {
            handleRouting(beacons)
        }
    }

    // Helpers
    fun hasConnectivity(): Boolean {
        var isConnected: Boolean = true
        if (!activity.userHasInternet() && !activity.userHasBluetooth()) {
            isConnected = false
            status = RouteHandler.STATUS.NO_BLUETOOTH_CONNECTIVITY
            this.beaconLocator.stopDetectingBeacons()
            routeHandleListener?.onLostConnectivity(status)
        } else if (!activity.userHasInternet()) {
            isConnected = false
            status = RouteHandler.STATUS.NO_CONNECTIVITY
            this.beaconLocator.stopDetectingBeacons()
            routeHandleListener?.onLostConnectivity(status)
        } else if (!activity.userHasBluetooth()) {
            isConnected = false
            status = RouteHandler.STATUS.NO_BLUETOOTH
            this.beaconLocator.stopDetectingBeacons()
            routeHandleListener?.onLostConnectivity(status)
        } else if (lastMagneticField >= 80.0) {
            isConnected = false
            lastMagneticField = 0f
            status = RouteHandler.STATUS.HIGH_MAGNETIC_FIELD
            this.beaconLocator.stopDetectingBeacons()
            routeHandleListener?.onLostConnectivity(status)
        }
        return isConnected
    }

    fun currentStep(): BeaconRoute {
        return route.peek().peek()
    }

    fun isRouteFinished(): Boolean {
        return route.size <= 0
    }

    fun tryReconnect() {
        if (hasConnectivity()) {
            status = STATUS.FINDING_POSITION
            routeHandleListener?.onChangedStatus(status)
            this.beaconLocator.startDetectingBeacons()
        }
    }

    override fun getApplicationContext(): Context {
        return activity.applicationContext
    }

    override fun unbindService(service: ServiceConnection?) {
        activity.unbindService(service)
    }

    override fun bindService(intent: Intent?, service: ServiceConnection?, p2: Int): Boolean {
        return activity.bindService(intent, service, p2)
    }

    override fun onBeaconServiceConnect() {
        this.beaconLocator.onBeaconServiceConnect()
    }
}