package com.example.asus_pc.supermarkets.Fragments

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.example.asus_pc.supermarkets.Activities.EcoActivity
import com.example.asus_pc.supermarkets.Activities.RouteActivity
import com.example.asus_pc.supermarkets.Activities.SectionsActivity
import com.example.asus_pc.supermarkets.Adapters.ListsAdapter
import com.example.asus_pc.supermarkets.Entities.ListEntity
import com.example.asus_pc.supermarkets.Helpers.EcoPreferences
import com.example.asus_pc.supermarkets.Interfaces.IListItemListener
import com.example.asus_pc.supermarkets.Managers.ListManager
import com.example.asus_pc.supermarkets.R
import com.example.asus_pc.supermarkets.Tasks.UIMainThread
import kotlinx.android.synthetic.main.fragment_list.*
import kotlinx.android.synthetic.main.fragment_list.view.*
import java.util.*

class ListFragment : EcoFragment(), View.OnClickListener, TextWatcher, IListItemListener {

    companion object {
        val CREATED_LIST_RESULT_CODE = 5
    }

    val DELAY: Long = 1000

    lateinit var adapter: ListsAdapter
    lateinit var txt_lists_search: EditText
    lateinit var txt_error_message: TextView
    lateinit var btn_add_new_list: Button
    var ecoActivity: EcoActivity? = null
    var timer: Timer = Timer()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.fragment_list, container, false)

        view.lists_recycler_view.layoutManager = LinearLayoutManager(view.context, LinearLayoutManager.VERTICAL, false)
        adapter = ListsAdapter(ArrayList<ListEntity>(), this)
        view.lists_recycler_view.adapter = adapter

        btn_add_new_list = view.btn_add_new_list
        btn_add_new_list.setOnClickListener(this)

        txt_lists_search = view.txt_lists_search
        txt_lists_search.addTextChangedListener(this)

        txt_error_message = view.txt_error_message

        return view
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == ListFragment.CREATED_LIST_RESULT_CODE && resultCode == ListFragment.CREATED_LIST_RESULT_CODE) {
            txt_lists_search.setText("")
            txt_lists_search.clearFocus()
            loadLists("")
        }
    }

    override fun onStop() {
        super.onStop()
    }

    override fun onStart() {
        super.onStart()
        if (ecoActivity == null) {
            ecoActivity = activity as EcoActivity
        }
        loadLists("")
    }

    // Helpers
    fun loadLists(q: String) {
        val preferences = EcoPreferences(activity!!.applicationContext)
        val superMarketId = preferences.getSupermarketId()
        if(superMarketId == null) return

        if (q.trim().equals("")) {
            ListManager.getLists(ecoActivity!!.applicationContext, superMarketId, { result ->
                UIMainThread.run(ecoActivity, {
                    adapter.lists = result
                    adapter.notifyDataSetChanged()
                    if(result.size <= 0) {
                        val text = getString(R.string.lists_fragment_error_no_lists_saved)
                        txt_error_message.setText(text)
                        ecoActivity?.speak(text, false)
                        ecoActivity?.speak("Haz click en la parte superior de la pantalla para crear una lista", false)
                        changeVisibility(txt_error_message, View.VISIBLE)
                    } else {
                        speakHowManyListsFound(result.size)
                        changeVisibility(txt_error_message, View.GONE)
                    }
                })
            })
        } else {
            ListManager.searchByName(ecoActivity!!.applicationContext, q, superMarketId, { result ->
                UIMainThread.run(ecoActivity, {
                    adapter.lists = result
                    adapter.notifyDataSetChanged()
                    if(result.size <= 0) {
                        val text = getString(R.string.lists_fragment_error_no_lists_found)
                        txt_error_message.setText(text)
                        ecoActivity?.speak(text)
                        changeVisibility(txt_error_message, View.VISIBLE)
                    } else {
                        speakHowManyListsFound(result.size)
                        changeVisibility(txt_error_message, View.GONE)
                    }
                })
            })
        }
    }

    fun speakHowManyListsFound(size: Int) {
        if(size == 1) {
            ecoActivity?.speak("Encontrada una lista", false)
        } else {
            ecoActivity?.speak("Encontradas {$size} listas", false)
        }
    }

    // Listeners
    override fun onClick(v: View?) {
        when (v) {
            btn_add_new_list -> {
                txt_lists_search.setText("")
                txt_lists_search.clearFocus()
                goToActivityForResultWithExtraString(SectionsActivity::class.java, ListFragment.CREATED_LIST_RESULT_CODE, SectionsActivity.LIST_MODE, SectionsActivity.LIST_MODE)
            }
        }
    }

    override fun OnClickListener(listItem: ListEntity) {
        val destinations = listItem.destinations
        ecoActivity?.goToActivityWithExtraStringArray(ecoActivity!!.applicationContext, RouteActivity::class.java, false, SectionsActivity.DESTINATIONS_KEY, destinations)
    }

    override fun OnLongClickListener(listItem: ListEntity) {
        ecoActivity?.showDialogWithTwoButton(getString(R.string.are_you_sure_dialog_title), getString(R.string.are_you_sure_dialog_delete_list_message), getString(R.string.are_you_sure_dialog_positive_button), DialogInterface.OnClickListener { dialog, which ->
            ListManager.deleteList(ecoActivity!!.applicationContext, listItem.id!!, {
                loadLists("")
            })
        }, getString(R.string.are_you_sure_dialog_negative_button), DialogInterface.OnClickListener { dialog, which -> })
    }

    override fun afterTextChanged(s: Editable) {
        if (s.length >= 3 || s.length == 0) {
            timer = Timer()
            timer.schedule(object : TimerTask() {
                override fun run() {
                    loadLists(s.toString())
                }

            }, DELAY)
        }
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        timer?.cancel()
    }
}