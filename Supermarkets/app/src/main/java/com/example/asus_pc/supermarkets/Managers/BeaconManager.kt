package com.example.asus_pc.supermarkets.Managers

import com.example.asus_pc.supermarkets.Interfaces.IBeaconManager
import com.example.asus_pc.supermarkets.Models.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*

object BeaconManager {

    val BASE_PATH: String = "https://us-central1-ecoserver-278bf.cloudfunctions.net"
    val client: IBeaconManager

    init {
        val builder: Retrofit.Builder = Retrofit.Builder()
                .baseUrl(this.BASE_PATH)
                .addConverterFactory(GsonConverterFactory.create())
        val retrofit: Retrofit = builder.build()
        client = retrofit.create(IBeaconManager::class.java)
    }

    // API
    fun calculateCurrentPosition(query: CalculatePositionQuery, callback: Callback<CategoryBeacon>) {
        val call: Call<CategoryBeacon> = client.calculateCurrentPosition(query)
        call.enqueue(callback)
    }

    fun getRoutingTable(supermarketId: String, startPoint: String, endPoint: String, callback: Callback<Array<BeaconRoute>>) {
        val call: Call<Array<BeaconRoute>> = client.getRoutingTable(supermarketId, startPoint, endPoint)
        call.enqueue(callback)
    }

    fun getMultipleRoutingTable(query: MultipleRoutingTableQuery, callback: Callback<Array<Array<BeaconRoute>>>) {
        val call: Call<Array<Array<BeaconRoute>>> = client.getMultipleRoutingTable(query)
        call.enqueue(callback)
    }

    // Helpers
    fun arrayToRouteOfRoutes(routes: Array<Array<BeaconRoute>>) : Queue<Queue<BeaconRoute>> {
        val routeOfRoute : Queue<Queue<BeaconRoute>> = LinkedList<Queue<BeaconRoute>>()
        lateinit var  smallRoute : Queue<BeaconRoute>
        for(bigRoute in routes) {
            smallRoute = LinkedList<BeaconRoute>()
            for(route in bigRoute) {
                smallRoute.add(route)
            }
            routeOfRoute.add(smallRoute)
        }
        return routeOfRoute
    }
}