package com.example.asus_pc.supermarkets.Fragments

import android.content.Intent
import android.os.Bundle
import android.speech.tts.TextToSpeech
import android.view.LayoutInflater
import android.view.SurfaceView
import android.view.View
import android.view.ViewGroup
import com.example.asus_pc.supermarkets.Activities.EcoActivity
import com.example.asus_pc.supermarkets.Helpers.CameraRecognizer
import com.example.asus_pc.supermarkets.Helpers.EcoPreferences
import com.example.asus_pc.supermarkets.Interfaces.ICameraRecognizer
import com.example.asus_pc.supermarkets.Managers.AnaliticsManager
import com.example.asus_pc.supermarkets.Managers.ProductsManager
import com.example.asus_pc.supermarkets.Models.Product
import com.example.asus_pc.supermarkets.R
import com.example.asus_pc.supermarkets.Tasks.UIMainThread
import kotlinx.android.synthetic.main.fragment_be_my_eyes.*
import kotlinx.android.synthetic.main.fragment_be_my_eyes.view.*


class BeMyEyesFragment : EcoFragment(), ICameraRecognizer<Product> {

    lateinit var cameraRecognizer: CameraRecognizer<Product>
    var rootView: View? = null
    var lastRecognizedProductName: String = ""
    var ecoActivity : EcoActivity? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        rootView = inflater.inflate(R.layout.fragment_be_my_eyes, container, false)
        initializeCamera(rootView!!.beMyEyesSurface)
        return rootView
    }

    override fun onStart() {
        super.onStart()
        if (ecoActivity == null) {
            ecoActivity = activity as EcoActivity
        }
        ecoActivity?.speak("Lee  productos con tu cámara", false)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
    }

    // Helpers
    fun initializeCamera(view: SurfaceView) {
        cameraRecognizer = CameraRecognizer(activity as EcoActivity, this, view, { item, dictionary ->
            var nameLength = 0
            val minimumCharDifference = 0.75
            for (product in dictionary) {
                nameLength = product.name.length
                if (item.length >= nameLength*minimumCharDifference && product.name.equals(item, ignoreCase = true) || item.contains(product.name, ignoreCase = true))
                    return@CameraRecognizer product
            }
            return@CameraRecognizer null
        })
        cameraRecognizer.setupCameraSource()
        val preferences = EcoPreferences(activity as EcoActivity)
        val superMarketId = preferences.getSupermarketId()
        if (superMarketId != null) {
            ProductsManager.findProductsFromSupermarket(superMarketId, { response ->
                cameraRecognizer.dictionary = response
            }, {
                println("EcoLogger: Error obteniendo los productos")
            })
        }
    }

    // Listeners
    override fun recognizedItem(item: Product) {
        println("EcoLogger: Recognized ${item.name}")
        rootView?.let { view ->
            if(!item.name.equals(lastRecognizedProductName, ignoreCase = true)) {
                ecoActivity?.speak("${item.name} con precio ${item.formatPrice()}, ${item.description}")
                lastRecognizedProductName = item.name
                AnaliticsManager.registerProductSearched(item.id)
            }

            UIMainThread.setText(activity, view.txtBeMyEyesProductName, item.name.capitalize())
            UIMainThread.setText(activity, view.txtBeMyEyesProductPrice, item.formatPrice())
            UIMainThread.loadImage(activity, item.imageUrl, view.txtBeMyEyesProductImage)
        }
    }

    override fun onErrorSetingUp() {
    }
}
