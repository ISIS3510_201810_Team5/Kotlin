package com.example.asus_pc.supermarkets.Fragments

import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.net.Uri

import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.asus_pc.supermarkets.Activities.EcoActivity
import com.example.asus_pc.supermarkets.Activities.RouteActivity
import com.example.asus_pc.supermarkets.Helpers.BeaconLocator
import com.example.asus_pc.supermarkets.Helpers.EcoPreferences
import com.example.asus_pc.supermarkets.Interfaces.IBeaconLocator
import com.example.asus_pc.supermarkets.Interfaces.IRouteAssistance
import com.example.asus_pc.supermarkets.Managers.BeaconManager


import com.example.asus_pc.supermarkets.Models.Message
import com.example.asus_pc.supermarkets.Managers.MessageManager
import com.example.asus_pc.supermarkets.Models.CalculatePositionQuery
import com.example.asus_pc.supermarkets.Models.CategoryBeacon

import com.example.asus_pc.supermarkets.R
import com.google.firebase.firestore.ListenerRegistration
import kotlinx.android.synthetic.main.fragment_call_assistance.view.*
import org.altbeacon.beacon.Beacon
import org.altbeacon.beacon.Region
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.ArrayList
import android.R.attr.delay
import com.squareup.picasso.Picasso


class CallAssistanceFragment : EcoFragment(), View.OnClickListener, IBeaconLocator {

    enum class STATUS {
        NO_ACTION,
        FINDING_POSITION,
        NO_BLUETOOTH,
        NO_CONNECTIVITY,
        NO_POSITION_FOUND,
        NO_BLUETOOTH_CONNECTIVITY
    }

    val VERIFY_CONNECTION_TIMEOUT: Long = 2000

    lateinit var preferences: EcoPreferences
    lateinit var rootView: View
    var snapshotListener: ListenerRegistration? = null
    var beaconLocator: BeaconLocator? = null
    var status: STATUS = STATUS.NO_ACTION
    var listener: IRouteAssistance? = null
    var comingHandler: Handler = Handler();
    var ecoActivity: EcoActivity? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.fragment_call_assistance, container, false)
        view.btn_ask_help.setOnClickListener(this)
        this.beaconLocator = BeaconLocator(this)
        setupFragment()
        view.btn_cancel_assistance.setOnClickListener(this)
        view.btn_call_supermarket.setOnClickListener(this)
        this.rootView = view
        return view
    }

    override fun onStart() {
        super.onStart()
        if (ecoActivity == null) {
            ecoActivity = activity as EcoActivity
        }
        val text = getString(R.string.call_assistance_fragment_use_phone)
        ecoActivity?.speak(text, false)
    }

    override fun onStop() {
        super.onStop()
        snapshotListener?.remove()
        this.beaconLocator?.stopDetectingBeacons()
    }

    fun setupFragment() {
        val context = this.activity!!.applicationContext
        preferences = EcoPreferences(context)
        val savedAssistanceId = preferences.getAssistanceId()
        if (savedAssistanceId != null) {
            findAlreadyCreatedMessage(savedAssistanceId)
        }
    }

    // Listeners
    override fun onClick(v: View?) {
        when (v) {
            rootView.btn_ask_help -> {
                if (hasConnectivity()) {
                    if (this.status == STATUS.NO_ACTION) {
                        saveInfo()
                    } else {
                        val context = this.activity!!.applicationContext
                        preferences = EcoPreferences(context)
                        val savedAssistanceId = preferences.getAssistanceId()
                        if (savedAssistanceId != null) {
                            findAlreadyCreatedMessage(savedAssistanceId)
                        } else {
                            this.status = STATUS.FINDING_POSITION
                            setupStatus()
                            this.beaconLocator?.startDetectingBeacons()
                        }
                    }
                }
            }
            rootView.btn_cancel_assistance -> {
                ecoActivity?.speak("Asistencia cancelada")
                preferences.removeAssistanceId()
                this.listener?.onCancelAssistance()
                status = STATUS.NO_ACTION
                setupStatus()
            }
            rootView.btn_call_supermarket -> {
                val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel:" + preferences.getSupermarketPhone()))
                startActivity(intent)
            }
        }
    }

    // Calculate position
    override fun didRangeBeaconsInRegion(beacons: Collection<Beacon>, region: Region) {
        hasConnectivity()
        calculateCurrentPosition(beacons)
    }

    override fun getApplicationContext(): Context {
        return activity!!.applicationContext
    }

    override fun unbindService(service: ServiceConnection?) {
        activity?.unbindService(service)
    }

    override fun bindService(intent: Intent?, service: ServiceConnection?, p2: Int): Boolean {
        if (activity != null) {
            return activity!!.bindService(intent, service, p2)
        }
        return false
    }

    override fun onBeaconServiceConnect() {
        this.beaconLocator?.onBeaconServiceConnect()
    }

    // Helpers
    fun saveInfo() {
        this.beaconLocator?.startDetectingBeacons()
        this.status = STATUS.FINDING_POSITION
        setupStatus()
    }

    fun callAssistance(sectionId: String) {
        val preferences = EcoPreferences(ecoActivity!!)
        val superMarketId = preferences.getSupermarketId()
        if (superMarketId != null) {
            MessageManager.createMessage(superMarketId, sectionId, { id ->
                preferences.saveAssistanceId(id)
                snapshotListener = MessageManager.listenToChanges(id, { result ->
                    handleAssistanceStatusChange(result)
                })
            }, {
                println("EcoLogger: Error al crear mensaje")
            })
        }
    }

    fun calculateCurrentPosition(beacons: Collection<Beacon>) {
        this.beaconLocator?.stopDetectingBeacons()
        // Geat near beacons
        val categoryBeacons: ArrayList<String> = ArrayList<String>()
        var newBeacon: CategoryBeacon? = null
        for (b in beacons) {
            newBeacon = CategoryBeacon(b)
            if (newBeacon.distance <= 1.0) {
                categoryBeacons.add(newBeacon.uuid)
            }
        }
        val uuids: Array<String> = categoryBeacons.toTypedArray()
        val preferences = EcoPreferences(ecoActivity!!)
        val superMarketId = preferences.getSupermarketId()
        if (superMarketId != null) {
            val query: CalculatePositionQuery = CalculatePositionQuery(superMarketId, uuids)
            BeaconManager.calculateCurrentPosition(query, object : Callback<CategoryBeacon> {
                override fun onResponse(call: Call<CategoryBeacon>?, response: Response<CategoryBeacon>?) {
                    if (response != null && response.isSuccessful) {
                        println("EcoLogger: Se encontro mi posicion")
                        callAssistance(response.body().sectionId)
                    } else {
                        // Mostrar error
                        status = STATUS.NO_POSITION_FOUND
                        setupStatus()
                    }
                }

                override fun onFailure(call: Call<CategoryBeacon>?, t: Throwable?) {
                    println("EcoLogger: Hubo un error encontrando mi posicion")
                    status = STATUS.NO_POSITION_FOUND
                    setupStatus()
                }
            })
        }
    }

    fun handleAssistanceStatusChange(message: Message) {
        val status = message.status
        when (status) {
            Message.STATUS.PENDING -> {
                val text = getString(R.string.call_assistance_fragment_wait_confirmation)
                rootView.txt_call_assitance.setText(text)
                ecoActivity?.speak(text)
                changeVisibility(rootView.assistance_progress_bar, View.VISIBLE)
                changeVisibility(rootView.assistance_image, View.GONE)
                changeVisibility(rootView.image_attending_profile, View.GONE)
                changeVisibility(rootView.btn_call_supermarket, View.GONE)
                changeVisibility(rootView.btn_cancel_assistance, View.VISIBLE)
                comingHandler.postDelayed(object : Runnable {
                    override fun run() {
                        if (activity != null && status == Message.STATUS.PENDING && hasConnectivity()) {
                            comingHandler.postDelayed(this, VERIFY_CONNECTION_TIMEOUT)
                        }
                    }
                }, VERIFY_CONNECTION_TIMEOUT)
            }
            Message.STATUS.COMING -> {
                var text = getString(R.string.call_assistance_fragment_assistant_coming_default)
                if (message.atiende != null) {
                    text = getString(R.string.call_assistance_fragment_assistant_coming_custom, message.atiende)
                }
                rootView.txt_call_assitance.setText(text)
                ecoActivity?.speak(text)
                if (message.foto != null) {
                    Picasso.get().load(message.foto).placeholder(R.drawable.groceries).into(rootView.image_attending_profile)
                }
                changeVisibility(rootView.image_attending_profile, View.VISIBLE)
                changeVisibility(rootView.assistance_progress_bar, View.GONE)
                changeVisibility(rootView.assistance_image, View.GONE)
                changeVisibility(rootView.btn_call_supermarket, View.GONE)
                changeVisibility(rootView.btn_cancel_assistance, View.VISIBLE)
            }
            Message.STATUS.DONE -> {
                ecoActivity?.speak("Asistencia completada")
                changeVisibility(rootView.btn_cancel_assistance, View.GONE)
                this.status = STATUS.NO_ACTION
                setupStatus()
            }
            Message.STATUS.CANCELLED -> {
                ecoActivity?.speak("Asistencia cancelada")
                changeVisibility(rootView.btn_cancel_assistance, View.GONE)
                this.status = STATUS.NO_ACTION
                setupStatus()
            }
        }
    }

    fun setupStatus() {
        if (this.status == STATUS.NO_ACTION) {
            resetToStartingState()
        } else if (this.status == STATUS.FINDING_POSITION) {
            changeVisibility(rootView.assistance_progress_bar, View.VISIBLE)
            changeVisibility(rootView.assistance_image, View.GONE)
            changeVisibility(rootView.image_attending_profile, View.GONE)
            changeVisibility(rootView.btn_cancel_assistance, View.GONE)
            changeVisibility(rootView.btn_call_supermarket, View.GONE)
            val text = getString(R.string.route_activity_finding_position)
            rootView.txt_call_assitance.setText(text)
            ecoActivity?.speak(text)
        } else if (this.status == STATUS.NO_BLUETOOTH) {
            changeVisibility(rootView.assistance_progress_bar, View.GONE)
            changeVisibility(rootView.assistance_image, View.VISIBLE)
            changeVisibility(rootView.image_attending_profile, View.GONE)
            changeVisibility(rootView.btn_cancel_assistance, View.GONE)
            changeVisibility(rootView.btn_call_supermarket, View.VISIBLE)
            rootView.assistance_image.setImageResource(R.drawable.ic_warning_light)
            val text = getString(R.string.error_no_bluetooth)
            rootView.txt_call_assitance.setText(getString(R.string.error_no_bluetooth))
            ecoActivity?.speak(text)
        } else if (this.status == STATUS.NO_CONNECTIVITY) {
            changeVisibility(rootView.assistance_progress_bar, View.GONE)
            changeVisibility(rootView.assistance_image, View.VISIBLE)
            changeVisibility(rootView.image_attending_profile, View.GONE)
            changeVisibility(rootView.btn_cancel_assistance, View.GONE)
            changeVisibility(rootView.btn_call_supermarket, View.VISIBLE)
            rootView.assistance_image.setImageResource(R.drawable.ic_connectivity_light)
            val text = getString(R.string.error_no_connectivity)
            rootView.txt_call_assitance.setText(getString(R.string.error_no_connectivity))
            ecoActivity?.speak(text)
        } else if (this.status == STATUS.NO_BLUETOOTH_CONNECTIVITY) {
            changeVisibility(rootView.assistance_progress_bar, View.GONE)
            changeVisibility(rootView.assistance_image, View.VISIBLE)
            changeVisibility(rootView.image_attending_profile, View.GONE)
            changeVisibility(rootView.btn_cancel_assistance, View.GONE)
            changeVisibility(rootView.btn_call_supermarket, View.VISIBLE)
            rootView.assistance_image.setImageResource(R.drawable.ic_warning_light)
            val text = getString(R.string.error_no_bluetooth_connectivity)
            rootView.txt_call_assitance.setText(getString(R.string.error_no_bluetooth_connectivity))
            ecoActivity?.speak(text)
        } else if (this.status == STATUS.NO_POSITION_FOUND) {
            changeVisibility(rootView.assistance_progress_bar, View.GONE)
            changeVisibility(rootView.assistance_image, View.VISIBLE)
            changeVisibility(rootView.image_attending_profile, View.GONE)
            changeVisibility(rootView.btn_cancel_assistance, View.GONE)
            changeVisibility(rootView.btn_call_supermarket, View.VISIBLE)
            rootView.assistance_image.setImageResource(R.drawable.ic_warning_light)
            val text = getString(R.string.route_activity_no_position)
            rootView.txt_call_assitance.setText(getString(R.string.route_activity_no_position))
            ecoActivity?.speak(text)
        }
    }

    fun resetToStartingState() {
        preferences.removeAssistanceId()
        changeVisibility(rootView.assistance_progress_bar, View.GONE)
        changeVisibility(rootView.image_attending_profile, View.GONE)
        changeVisibility(rootView.image_attending_profile, View.GONE)
        changeVisibility(rootView.assistance_image, View.VISIBLE)
        changeVisibility(rootView.btn_cancel_assistance, View.GONE)
        changeVisibility(rootView.btn_call_supermarket, View.GONE)
        rootView.assistance_image.setImageResource(R.drawable.ic_phone_light)
        rootView.txt_call_assitance.setText(getString(R.string.call_assistance_fragment_use_phone))
    }


    fun hasConnectivity(): Boolean {
        var isConnected: Boolean = true
        if (!ecoActivity!!.userHasInternet() && !ecoActivity!!.userHasBluetooth()) {
            isConnected = false
            status = STATUS.NO_BLUETOOTH_CONNECTIVITY
            this.beaconLocator?.stopDetectingBeacons()
            this.snapshotListener?.remove()
            setupStatus()
        } else if (!ecoActivity!!.userHasInternet()) {
            isConnected = false
            status = STATUS.NO_CONNECTIVITY
            this.snapshotListener?.remove()
            this.beaconLocator?.stopDetectingBeacons()
            setupStatus()
        } else if (!ecoActivity!!.userHasBluetooth()) {
            isConnected = false
            status = STATUS.NO_BLUETOOTH
            this.snapshotListener?.remove()
            this.beaconLocator?.stopDetectingBeacons()
            setupStatus()
        }
        return isConnected
    }

    fun findAlreadyCreatedMessage(messageId: String) {
        MessageManager.findMessageById(messageId, { result ->
            snapshotListener = MessageManager.listenToChanges(result.id, { result ->
                handleAssistanceStatusChange(result)
            })
        }, {
            println("EcoLogger: Error al buscar mensaje")
        })
    }
}
