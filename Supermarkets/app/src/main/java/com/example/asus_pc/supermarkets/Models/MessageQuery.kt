package com.example.asus_pc.supermarkets.Models

import com.google.firebase.firestore.DocumentReference
import java.util.*

class MessageQuery {
    val status : Long = Message.statusEnumToLong(Message.STATUS.PENDING)
    var section : DocumentReference? = null
    var createdAt: Date = Date()
}