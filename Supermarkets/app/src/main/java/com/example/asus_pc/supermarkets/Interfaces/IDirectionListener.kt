package com.example.asus_pc.supermarkets.Interfaces

interface IDirectionListener {
    fun onRotationChanged(azimuth: Float, pitch: Float, oldDegree: Float)
    fun onMagneticFieldChanged(value: Float)
}