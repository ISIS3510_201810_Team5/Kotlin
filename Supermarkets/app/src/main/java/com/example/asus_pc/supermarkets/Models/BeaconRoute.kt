package com.example.asus_pc.supermarkets.Models

import android.widget.ImageView
import com.example.asus_pc.supermarkets.R
import org.altbeacon.beacon.Beacon
import java.util.*

class BeaconRoute {

    val from: String
    val to: String
    var distance: Int
    val direction: String
    val unit: String
    val bearing: Int

    constructor(from: String, to: String, distance: Int, direction: String, unit: String, bearing: Int) {
        this.from = from
        this.to = to
        this.distance = distance
        this.direction = direction
        this.unit = unit
        this.bearing = bearing
    }

    override fun toString(): String {
        return "Camina hacia ${getDirectionString()}, $distance $unit"
    }

    fun getImage() : Int {
        when(direction.toUpperCase()) {
            "FORWARD" -> {
                return getForwardImage()
            }
            "RIGHT" -> {
                return R.drawable.ic_arrow_right_light
            }
            "LEFT" -> {
                return R.drawable.ic_arrow_left_light
            }
            "BACKWARDS" -> {
                return R.drawable.ic_arrow_backward_light
            }
        }
        return R.drawable.ic_arrived_light
    }

    fun getForwardImage() : Int {
        return R.drawable.ic_arrow_forward_light
    }

    fun getDirectionString() : String {
        when(direction.toUpperCase()) {
            "FORWARD" -> {
                return "adelante"
            }
            "LEFT" -> {
                return "la izquierda"
            }
            "RIGHT" -> {
                return "la derecha"
            }
            "BACKWARDS" -> {
                return "atras"
            }
        }
        return ""
    }
}