package com.example.asus_pc.supermarkets.Interfaces

interface ICameraRecognizer<Type> {
    fun recognizedItem(item: Type)
    fun onErrorSetingUp()
}