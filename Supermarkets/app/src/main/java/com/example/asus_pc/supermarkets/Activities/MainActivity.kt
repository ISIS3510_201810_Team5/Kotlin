package com.example.asus_pc.supermarkets.Activities

import android.os.Bundle
import android.speech.tts.TextToSpeech
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.view.MenuItem
import com.example.asus_pc.supermarkets.Fragments.BeMyEyesFragment
import com.example.asus_pc.supermarkets.Fragments.CallAssistanceFragment
import com.example.asus_pc.supermarkets.Fragments.HomeFragment
import com.example.asus_pc.supermarkets.Fragments.ListFragment
import com.example.asus_pc.supermarkets.Helpers.EcoPreferences
import com.example.asus_pc.supermarkets.Helpers.EcoSpeechRecognizer
import com.example.asus_pc.supermarkets.Interfaces.IEcoSpeechRecognizer
import com.example.asus_pc.supermarkets.R
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_home.view.*

class MainActivity : EcoActivity(), BottomNavigationView.OnNavigationItemSelectedListener, IEcoSpeechRecognizer {

    val manager = supportFragmentManager
    var preferences: EcoPreferences? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        replaceFragment(HomeFragment())
        bottom_navigation_view.setOnNavigationItemSelectedListener(this)
        EcoSpeechRecognizer.runRecognizerSetup(this)
        askAppPermissions({},{})
        preferences = EcoPreferences(this)

        this.initializeTextToSpeech(this, {
            val supermarketName = preferences?.getSupermarketName()
            var text = getString(R.string.home_fragment_default_welcome_message)
            if(supermarketName != null) {
                text = getString(R.string.home_fragment_custom_welcome_message, supermarketName)
            }
            speak(text)
        })
    }

    override fun onStart() {
        super.onStart()
        if(ecoTalker == null) {
            this.initializeTextToSpeech(this, {})
        }
    }

    override fun onResume() {
        super.onResume()
        EcoSpeechRecognizer.subscribe(this)
    }

    override fun onPause() {
        super.onPause()
        EcoSpeechRecognizer.unsubscribe(this)
    }

    override fun onStop() {
        super.onStop()
        this.destroyTextToSpeech()
    }

    override fun onDestroy() {
        super.onDestroy()
        EcoSpeechRecognizer.unsubscribe(this)
        EcoSpeechRecognizer.destroy()
    }

    // Listeners
    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.fragment_home -> {
                speak("Vista de home")
                replaceFragment(HomeFragment())
                return true
            }
            R.id.fragment_be_my_eyes -> {
                speak("Vista de cámara")
                replaceFragment(BeMyEyesFragment())
                return true
            }
            R.id.fragment_call_assistance -> {
                speak("Vista de asistencia")
                replaceFragment(CallAssistanceFragment())
                return true
            }
            R.id.fragment_lists -> {
                speak("Vista de listas guardadas")
                replaceFragment(ListFragment())
                return true
            }
        }
        return false
    }

    override fun onResult(text: String) {
        when(text.toLowerCase()) {
            "buscar" -> {
                goToActivity(this, SectionsActivity::class.java)
            }
            "asistencia" -> {
                bottom_navigation_view.selectedItemId = R.id.fragment_call_assistance
            }
            "fotografía" -> {
                bottom_navigation_view.selectedItemId = R.id.fragment_be_my_eyes
            }
            "listas" -> {
                bottom_navigation_view.selectedItemId = R.id.fragment_lists
            }
        }
    }

    override fun onPartialResult(text: String) {
        println("EcoLogger: Se escucho \'" + text +"\'")
    }

    // Helpers
    fun replaceFragment(fragment: Fragment) {
        val transaction = manager.beginTransaction();
        transaction.replace(R.id.fragmet_holder, fragment);
        transaction.commit();
    }

    override fun onBackPressed() {
        val count = manager.backStackEntryCount
        if (count > 1) {
            super.onBackPressed();
        }
    }
}
