package com.example.asus_pc.supermarkets.Tasks

import android.content.Context
import android.media.MediaPlayer
import android.os.AsyncTask
import com.example.asus_pc.supermarkets.Helpers.EcoSpeechRecognizer
import com.example.asus_pc.supermarkets.R
import edu.cmu.pocketsphinx.Assets
import java.io.IOException
import java.lang.ref.WeakReference

class EcoSpeechRecognizerAsync(context: Context) : AsyncTask<Void, Void, Exception>() {

    val context: WeakReference<Context> = WeakReference<Context>(context)

    override fun doInBackground(vararg params: Void): Exception? {
        try {
            val assets = Assets(context.get())
            val assetDir = assets.syncAssets()

            val beepSound: MediaPlayer = MediaPlayer.create(context.get(), R.raw.eco_listen)
            EcoSpeechRecognizer.setupRecognizer(assetDir, beepSound)
        } catch (e: IOException) {
            return e
        }

        return null
    }

    override fun onPostExecute(result: Exception?) {
        if (result != null) {
            println(result.message)
        } else {
            EcoSpeechRecognizer.switchSearch(EcoSpeechRecognizer.KWS_SEARCH)
        }
    }
}