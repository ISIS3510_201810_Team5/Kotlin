package com.example.asus_pc.supermarkets.Fragments

import android.content.Intent
import android.support.v4.app.Fragment
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.example.asus_pc.supermarkets.Activities.EcoActivity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_be_my_eyes.view.*

open class EcoFragment : Fragment() {
    // Helpers
    fun goToActivity(cls: Class<*>, shouldFinishCurrentClass: Boolean = false) {
        val activity: EcoActivity = activity as EcoActivity
        val intent = Intent(activity.applicationContext, cls)
        startActivity(intent)
        if (shouldFinishCurrentClass) activity.finish();
    }

    fun goToActivityForResult(cls: Class<*>, requestCode: Int) {
        val activity: EcoActivity = activity as EcoActivity
        val intent = Intent(activity.applicationContext, cls)
        startActivityForResult(intent, requestCode)
    }

    fun goToActivityForResultWithExtraString(cls: Class<*>, requestCode: Int, extraKey: String, extra: String) {
        val activity: EcoActivity = activity as EcoActivity
        val intent = Intent(activity.applicationContext, cls)
        intent.putExtra(extraKey, extra)
        startActivityForResult(intent, requestCode)
    }

    fun changeVisibility(view: View, newVisibility: Int) {
        if (view.visibility != newVisibility) {
            view.visibility = newVisibility
        }
    }
}