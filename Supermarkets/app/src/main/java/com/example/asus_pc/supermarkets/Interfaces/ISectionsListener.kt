package com.example.asus_pc.supermarkets.Interfaces

interface ISectionsListener {
    fun onStartAvailable()
    fun onStartUnavailable()
}