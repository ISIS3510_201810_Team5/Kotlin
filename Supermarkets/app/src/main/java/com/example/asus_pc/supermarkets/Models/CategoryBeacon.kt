package com.example.asus_pc.supermarkets.Models

import org.altbeacon.beacon.Beacon

class CategoryBeacon {

    val uuid: String
    val distance: Double
    val sectionId: String

    constructor(uuid: String, distance: Double, sectionId: String) {
        this.uuid = uuid
        this.distance = distance
        this.sectionId = sectionId
    }

    constructor(beacon: Beacon) {
        val distance1 = beacon.distance
        this.distance = Math.round(distance1 * 100.0) / 100.0
        this.uuid = "${beacon.id1}"
        this.sectionId = ""
    }
}