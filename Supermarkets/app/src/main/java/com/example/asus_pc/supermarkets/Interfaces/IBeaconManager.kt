package com.example.asus_pc.supermarkets.Interfaces

import com.example.asus_pc.supermarkets.Models.BeaconRoute
import com.example.asus_pc.supermarkets.Models.CalculatePositionQuery
import com.example.asus_pc.supermarkets.Models.CategoryBeacon
import com.example.asus_pc.supermarkets.Models.MultipleRoutingTableQuery
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface IBeaconManager {
    @GET("/routingTable")
    fun getRoutingTable(@Query("supermarket_id") supermarketId: String, @Query("startPoint") startPoint: String, @Query("endPoint") endPoint: String): Call<Array<BeaconRoute>>

    @POST("/calculateCurrentPosition")
    fun calculateCurrentPosition(@Body beacons: CalculatePositionQuery): Call<CategoryBeacon>

    @POST("/multipleRoutingTable")
    fun getMultipleRoutingTable(@Body sectionsSupermarket: MultipleRoutingTableQuery): Call<Array<Array<BeaconRoute>>>
}