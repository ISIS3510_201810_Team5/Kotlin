package com.example.asus_pc.supermarkets.Adapters

import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.asus_pc.supermarkets.Activities.EcoActivity
import com.example.asus_pc.supermarkets.Activities.RouteActivity
import com.example.asus_pc.supermarkets.Interfaces.ISectionsListener
import com.example.asus_pc.supermarkets.Models.Section
import com.example.asus_pc.supermarkets.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_list_sections.view.*

class SectionsAdapter(var sectionsList: ArrayList<Section>, val listener: ISectionsListener) : RecyclerView.Adapter<SectionsAdapter.ViewHolder>() {

    var selection: ArrayList<Section> = ArrayList<Section>()
    var previousSelectionSize: Int = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_list_sections, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return sectionsList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val category: Section = sectionsList[position]
        // Set views
        holder.itemView.item_list_sections_title.text = category.name.capitalize()
        Picasso.get().load(category.imageUrl).resize(600,500).placeholder(R.drawable.groceries).into(holder.itemView.item_list_sections_image)

        checkIfAlredySelected(category, holder.itemView)

        holder.itemView.setOnClickListener {
            previousSelectionSize = selection.size
            if (selection.contains(category)) {
                selection.remove(category)
                unselectItem(category, holder.itemView)
            } else {
                selection.add(category)
                selectItem(category, holder.itemView)
            }

            if (selection.size == 1 && previousSelectionSize == 0) {
                listener.onStartAvailable()
            } else if (selection.size == 0) {
                listener.onStartUnavailable()
            }
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    }

    // Helpers
    fun selectItem(section: Section, itemView: View) {
        itemView.setBackgroundColor(Color.LTGRAY)
    }

    fun unselectItem(section: Section, itemView: View) {
        itemView.setBackgroundColor(Color.TRANSPARENT)
    }

    fun checkIfAlredySelected(section: Section, itemView: View) {
        for(selected in selection) {
            if(selected.beaconUuid.equals(section.beaconUuid, ignoreCase = true) || selected.id.equals(section.id, ignoreCase = true) ) {
                selectItem(section, itemView)
                return
            }
        }
        unselectItem(section, itemView)
    }
}