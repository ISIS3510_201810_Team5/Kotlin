package com.example.asus_pc.supermarkets.Models

class MultipleRoutingTableQuery {
    val supermarket_id: String
    val origin: String
    val sections: Array<String>

    constructor(supermarket_id: String, origin: String, sections: Array<String>) {
        this.supermarket_id = supermarket_id
        this.origin = origin
        this.sections = sections
    }
}