package com.example.asus_pc.supermarkets.Helpers

import android.bluetooth.BluetoothAdapter
import android.os.RemoteException
import android.util.Log
import com.example.asus_pc.supermarkets.Interfaces.IBeaconLocator
import org.altbeacon.beacon.*

class BeaconLocator : RangeNotifier {

    private val ALL_BEACONS_REGION = "AllBeaconsRegion"
    protected val TAG = "BEACON_LOCATOR"
    private val DEFAULT_SCAN_PERIOD_MS = 2000L

    private val beaconCosumer: IBeaconLocator
    private val beaconManager: BeaconManager
    private val identifiers: ArrayList<Identifier>
    private val region: Region

    constructor(beaconCosumer: IBeaconLocator) {
        this.beaconCosumer = beaconCosumer
        this.beaconManager = BeaconManager.getInstanceForApplication(beaconCosumer.applicationContext)
        this.beaconManager.beaconParsers.add(BeaconParser()
                .setBeaconLayout("m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24,d:25-25;m:0-3=4c000215"))
        this.identifiers = ArrayList()
        this.region = Region(ALL_BEACONS_REGION, identifiers)
    }

    // Listeners
    fun onBeaconServiceConnect() {
        try {
            println("EcoLogger: Se conecto")
            beaconManager.startRangingBeaconsInRegion(this.region)
        } catch (e: RemoteException) {
            Log.d(TAG, "Se ha producido una excepción al empezar a buscar beacons " + e.message)
        }
        beaconManager.addRangeNotifier(this)
    }

    override fun didRangeBeaconsInRegion(beacons: MutableCollection<Beacon>, region: Region) {
        this.beaconCosumer.didRangeBeaconsInRegion(beacons, region)
    }

    // Helpers
    fun startDetectingBeacons() {
        println("EcoLogger: Se empezo")
        this.beaconManager.foregroundScanPeriod = DEFAULT_SCAN_PERIOD_MS
        this.beaconManager.bind(this.beaconCosumer)
    }

    fun stopDetectingBeacons() {
        println("EcoLogger: Se termino")
        try {
            this.beaconManager.stopMonitoringBeaconsInRegion(this.region)
        } catch (e: RemoteException) {
            Log.d(TAG, "Se ha producido una excepción al empezar a buscar beacons " + e.message)
        }
        this.destroy()
    }

    fun destroy() {
        println("EcoLogger: Se destruyo")
        this.beaconManager.removeAllRangeNotifiers()
        this.beaconManager.unbind(this.beaconCosumer)
    }
}