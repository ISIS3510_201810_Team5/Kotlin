package com.example.asus_pc.supermarkets.Activities

import android.os.Bundle
import android.view.Window
import com.example.asus_pc.supermarkets.R
import java.util.*

class SplashActivity : EcoActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_splash)
        Timer().schedule(object : TimerTask() {
            override fun run() {
                goToActivity(this@SplashActivity, SupermarketLocationActivity::class.java, true)
            }
        }, 1000L)
    }

}
