package com.example.asus_pc.supermarkets.Activities

import android.os.Bundle
import android.view.View
import com.example.asus_pc.supermarkets.R
import kotlinx.android.synthetic.main.activity_arrived.*

class ArrivedActivity : EcoActivity(), View.OnClickListener {

    companion object {
        val FINISH_ROUTE: String = "FINISH_ROUTE"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_arrived)
        btn_arrived_close.setOnClickListener(this)
        btn_continue_route.setOnClickListener(this)
        btn_be_my_eyes.setOnClickListener(this)

        val shouldFinishRoute = getExtraOptionalString(ArrivedActivity.FINISH_ROUTE)
        if (shouldFinishRoute != null) {
            // Hide be my eyes and continue route
            btn_be_my_eyes.visibility = View.GONE
            btn_continue_route.visibility = View.GONE
        } else {
            // Hide close
            btn_be_my_eyes.visibility = View.VISIBLE
            btn_continue_route.visibility = View.VISIBLE
            btn_arrived_close.visibility = View.GONE
        }
        this.initializeTextToSpeech(this, {
            if(shouldFinishRoute != null) {
                speak("Has llegado a tu destino final")
            } else {
                speak("Has llegado a tu destino")
            }
        })
    }

    override fun onDestroy() {
        super.onDestroy()
        this.destroyTextToSpeech()
    }

    override fun onClick(v: View?) {
        when (v) {
            btn_arrived_close -> {
                btn_arrived_close.isEnabled = false
                finish()
            }
            btn_continue_route -> {
                finish()
            }
            btn_be_my_eyes -> {
                goToActivity(this, RouteBeMyEyesActivity::class.java, false)
            }
        }
    }

    // Helpers
}