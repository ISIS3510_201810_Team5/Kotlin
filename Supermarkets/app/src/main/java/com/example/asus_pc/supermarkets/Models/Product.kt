package com.example.asus_pc.supermarkets.Models

class Product {

    val id: String
    val name: String
    val price: Money
    val description: String
    val imageUrl: String

    constructor(id: String, name: String, price: Money, description: String, imageUrl: String) {
        this.id = id
        this.name = name
        this.price = price
        this.description = description
        this.imageUrl = imageUrl
    }

    constructor(id: String, data: Map<String, Any>) {
        this.id = id
        this.name = data.get("name") as String
        this.price = Money(data.get("price") as Map<String, Any>)
        this.description = data.get("descripcion") as String
        this.imageUrl = data.get("imageUrl") as String
    }

    // Helpers
    fun formatPrice() : String {
        return "$${this.price.units} ${this.price.iso}"
    }
}