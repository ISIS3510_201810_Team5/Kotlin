package com.example.asus_pc.supermarkets.Interfaces

import org.altbeacon.beacon.Beacon
import org.altbeacon.beacon.BeaconConsumer
import org.altbeacon.beacon.Region

interface IBeaconLocator : BeaconConsumer {
    fun didRangeBeaconsInRegion(beacons: Collection<Beacon>, region: Region)
}