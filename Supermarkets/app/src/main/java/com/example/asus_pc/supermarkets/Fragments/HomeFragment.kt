package com.example.asus_pc.supermarkets.Fragments

import android.os.Bundle
import android.speech.tts.TextToSpeech
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.asus_pc.supermarkets.Activities.EcoActivity
import com.example.asus_pc.supermarkets.Activities.SectionsActivity
import com.example.asus_pc.supermarkets.Helpers.EcoPreferences
import com.example.asus_pc.supermarkets.R
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_home.view.*

class HomeFragment : EcoFragment(), View.OnClickListener {

    var preferences: EcoPreferences? = null
    var ecoActivity: EcoActivity? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.fragment_home, container, false)
        view.btn_search_section.setOnClickListener(this)

        preferences = EcoPreferences(activity!!.applicationContext)
        val supermarketName = preferences?.getSupermarketName()
        if(supermarketName != null) {
            view.txt_welcome_message.setText(getString(R.string.home_fragment_custom_welcome_message, supermarketName))
        } else {
            view.txt_welcome_message.setText(getString(R.string.home_fragment_default_welcome_message))
        }

        return view
    }

    override fun onStart() {
        super.onStart()
        if (ecoActivity == null) {
            ecoActivity = activity as EcoActivity
        }
    }

    override fun onStop() {
        super.onStop()
        btn_search_section.isEnabled = true
    }

    // Listeners
    override fun onClick(v: View?) {
        when(v) {
            btn_search_section -> {
                btn_search_section.isEnabled = false
                goToActivity(SectionsActivity::class.java)
            }
        }
    }
}