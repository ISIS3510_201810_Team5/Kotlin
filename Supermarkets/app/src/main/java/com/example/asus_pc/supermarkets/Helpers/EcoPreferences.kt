package com.example.asus_pc.supermarkets.Helpers

import android.content.Context
import android.content.SharedPreferences

class EcoPreferences(context: Context) {

    val PREFERENCE_NAME = "ECO_PREFERENCES"
    val ASSISTANCE_ID = "ASSISTANCE_ID"
    val SUPERMARKET_ID = "SUPERMARKET_ID"
    val SUPERMARKET_PHONE = "SUPERMARKET_PHONE"
    val SUPERMARKET_SHOULD_USE_DIRECTION = "SUPERMARKET_SHOULD_USE_DIRECTION"
    val SUPERMARKET_NAME = "SUPERMARKET_NAME"

    val prefs = context.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE)

    // Assistance
    fun getAssistanceId() : String? {
        return prefs.getString(ASSISTANCE_ID, null)
    }

    fun saveAssistanceId(assistanceId: String) {
        val editor = prefs.edit()
        editor.putString(ASSISTANCE_ID, assistanceId)
        editor.apply()
    }

    fun removeAssistanceId() {
        val editor = prefs.edit()
        editor.remove(ASSISTANCE_ID)
        editor.apply()
    }

    // Supermarket
    fun getSupermarketId() : String? {
        return prefs.getString(SUPERMARKET_ID, null)
    }

    fun getSupermarketPhone() : String? {
        return prefs.getString(SUPERMARKET_PHONE, null)
    }

    fun getSupermarketName() : String? {
        return prefs.getString(SUPERMARKET_NAME, null)
    }

    fun getShouldUseDirection() : Boolean {
        return prefs.getBoolean(SUPERMARKET_SHOULD_USE_DIRECTION, true)
    }

    fun saveSupermarketId(supermarketId: String) {
        val editor = prefs.edit()
        editor.putString(SUPERMARKET_ID, supermarketId)
        editor.apply()
    }

    fun saveSupermarketPhone(supermarketPhone: String) {
        val editor = prefs.edit()
        editor.putString(SUPERMARKET_PHONE, supermarketPhone)
        editor.apply()
    }

    fun saveShouldUseDirection(shouldUseDirection: Boolean?) {
        val editor = prefs.edit()
        val value = if(shouldUseDirection != null) shouldUseDirection else true
        editor.putBoolean(SUPERMARKET_SHOULD_USE_DIRECTION, value)
        editor.apply()
    }

    fun saveSupermarketName(supermarketName: String) {
        val editor = prefs.edit()
        editor.putString(SUPERMARKET_NAME, supermarketName)
        editor.apply()
    }
}