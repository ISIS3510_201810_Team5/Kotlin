package com.example.asus_pc.supermarkets.Helpers.Direction

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorManager
import com.example.asus_pc.supermarkets.Helpers.LowPassFilter
import kotlin.math.PI

class VectorDirectionProvider : DirectionStrategy {

    // Sensors
    val rotationSensor: Sensor

    // Data
    var lowPassFilter : LowPassFilter

    constructor(context: Context): super(context) {
        rotationSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR)
        lowPassFilter = LowPassFilter()
    }

    override fun start() {
        sensorManager.registerListener(this, rotationSensor, SensorManager.SENSOR_DELAY_GAME)
    }

    override fun onSensorChanged(event: SensorEvent?) {
        val eventType = event!!.sensor.type

        if (eventType != Sensor.TYPE_ROTATION_VECTOR) return

        var mOrientationData = FloatArray(3)

        calcOrientation(mOrientationData, event.values.clone())

        mOrientationData = lowPassFilter.lowPass(mOrientationData)

        val azimuth = mOrientationData[0]
        val pitch = mOrientationData[0]
        val roll = mOrientationData[1]

        onValueChangedListener.onRotationChanged(azimuth, pitch, roll)
    }

    // Helpers
    private fun calcOrientation(orientation: FloatArray, incomingValues: FloatArray) {
        // Get the quaternion
        val quatF = FloatArray(4)
        SensorManager.getQuaternionFromVector(quatF, incomingValues)

        // Get the rotation matrix
        //
        val rotMatF = Array(3) { FloatArray(3) }
        rotMatF[0][0] = quatF[1] * quatF[1] + quatF[0] * quatF[0] - 0.5f
        rotMatF[0][1] = quatF[1] * quatF[2] - quatF[3] * quatF[0]
        rotMatF[0][2] = quatF[1] * quatF[3] + quatF[2] * quatF[0]
        rotMatF[1][0] = quatF[1] * quatF[2] + quatF[3] * quatF[0]
        rotMatF[1][1] = quatF[2] * quatF[2] + quatF[0] * quatF[0] - 0.5f
        rotMatF[1][2] = quatF[2] * quatF[3] - quatF[1] * quatF[0]
        rotMatF[2][0] = quatF[1] * quatF[3] - quatF[2] * quatF[0]
        rotMatF[2][1] = quatF[2] * quatF[3] + quatF[1] * quatF[0]
        rotMatF[2][2] = quatF[3] * quatF[3] + quatF[0] * quatF[0] - 0.5f

        // Get the orientation of the phone from the rotation matrix
        val rad2deg = (180.0 / PI).toFloat()
        orientation[0] = Math.atan2((-rotMatF[1][0]).toDouble(), rotMatF[0][0].toDouble()).toFloat() * rad2deg
        orientation[1] = Math.atan2((-rotMatF[2][1]).toDouble(), rotMatF[2][2].toDouble()).toFloat() * rad2deg
        orientation[2] = Math.asin(rotMatF[2][0].toDouble()).toFloat() * rad2deg
        if (orientation[0] < 0) orientation[0] += 360f
    }
}