package com.example.asus_pc.supermarkets.Managers

import com.example.asus_pc.supermarkets.Models.Product
import com.example.asus_pc.supermarkets.Models.Section
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore

object ProductsManager {
    private val COLLECTION_NAME: String = "products"
    private val db: CollectionReference

    init {
        db = FirebaseFirestore.getInstance().collection(COLLECTION_NAME)
    }

    // Helpers
    fun getProductReference(productId: String): DocumentReference {
        return db.document(productId)
    }

    // API
    fun findProductsFromSupermarket(supermarketId: String, onSuccess: (ArrayList<Product>) -> Unit, onFailure: () -> Unit) {
        val supermarket = SupermarketsManager.getSurpermarketReference(supermarketId)
        db.whereArrayContains("supermarkets", supermarket)
                .orderBy("name")
                .get()
                .addOnCompleteListener(OnCompleteListener { task ->
                    if (task.isSuccessful) {
                        val results = task.result
                        val products = ArrayList<Product>()
                        for (result in results) {
                            val product: Map<String, Any> = result.data
                            products.add(Product(result.id, product))
                        }
                        onSuccess(products)
                    } else {
                        onFailure()
                    }
                })
    }
}