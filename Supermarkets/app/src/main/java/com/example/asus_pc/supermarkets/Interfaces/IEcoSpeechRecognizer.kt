package com.example.asus_pc.supermarkets.Interfaces

interface IEcoSpeechRecognizer {
    fun onResult(text: String)
    fun onPartialResult(text: String)
}