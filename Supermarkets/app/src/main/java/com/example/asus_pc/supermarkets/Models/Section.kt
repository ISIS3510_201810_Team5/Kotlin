package com.example.asus_pc.supermarkets.Models

class Section {

    val id: String
    val name: String
    val beaconUuid: String
    val imageUrl: String

    constructor(id: String, name: String, beaconUuid: String, imageUrl: String) {
        this.id = id
        this.name = name
        this.beaconUuid = beaconUuid
        this.imageUrl = imageUrl
    }

    constructor(id: String, data: Map<String, Any>) {
        this.id = id
        this.name = data.get("name") as String
        this.beaconUuid = data.get("beaconUuid") as String
        this.imageUrl = data.get("imageUrl") as String
    }

    // For comparing object purposes
    override fun hashCode(): Int {
        return this.beaconUuid.hashCode()
    }

    override fun equals(other: Any?): Boolean {
        return this.hashCode().equals(other?.hashCode())
    }
}