package com.example.asus_pc.supermarkets.Models

import com.google.firebase.firestore.GeoPoint

class Supermarket {

    val id: String
    val name: String
    val phone: String
    val location: GeoPoint
    val shouldUseDirection: Boolean

    constructor(id: String, name: String, phone: String, location: GeoPoint, shouldUseDirection: Boolean) {
        this.id = id
        this.name = name
        this.phone = phone
        this.location = location
        this.shouldUseDirection = shouldUseDirection
    }

    constructor(id: String, data: Map<String, Any>) {
        this.id = id
        this.name = data.get("name") as String
        this.phone = data.get("phone") as String
        this.location = data.get("location") as GeoPoint
        this.shouldUseDirection = data.get("shouldUseDirection") as Boolean
    }
}