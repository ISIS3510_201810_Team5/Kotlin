package com.example.asus_pc.supermarkets.Activities

import android.Manifest
import android.bluetooth.BluetoothAdapter
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import com.github.florent37.runtimepermission.kotlin.askPermission
import android.content.DialogInterface
import android.hardware.Sensor
import android.hardware.SensorManager
import android.support.v7.app.AlertDialog
import android.location.LocationManager
import com.example.asus_pc.supermarkets.R
import android.speech.tts.TextToSpeech
import android.text.InputType
import android.widget.EditText
import java.util.*


open class EcoActivity : AppCompatActivity() {

    var ecoTalker: TextToSpeech? = null

    // Listeners
    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return if (item?.itemId == android.R.id.home) {
            finish()
            return true
        } else {
            super.onOptionsItemSelected(item)
        }
    }

    // Helpers
    fun changeVisibility(view: View, newVisibility: Int) {
        if (view.visibility != newVisibility) {
            view.visibility = newVisibility
        }
    }

    fun showDialogWithOneButton(title: String, body: String, positiveButton: String, positivelistener: DialogInterface.OnClickListener) {
        val alert = AlertDialog.Builder(this).create()
        alert.setTitle(title)
        alert.setMessage(body)
        alert.setButton(AlertDialog.BUTTON_NEUTRAL, positiveButton, positivelistener)
        alert.setCanceledOnTouchOutside(false)
        if (!isFinishing) {
            alert.show()
        }
    }

    fun showDialogWithTwoButton(title: String, body: String, positiveButton: String, positivelistener: DialogInterface.OnClickListener, negativeButton: String, negativeListener: DialogInterface.OnClickListener) {
        val alert = AlertDialog.Builder(this).create()
        alert.setTitle(title)
        alert.setMessage(body)
        alert.setButton(AlertDialog.BUTTON_NEGATIVE, negativeButton, negativeListener)
        alert.setButton(AlertDialog.BUTTON_POSITIVE, positiveButton, positivelistener)
        alert.setCanceledOnTouchOutside(false)
        if (!isFinishing) {
            alert.show()
        }
    }

    fun showInputDialogWithOneButton(title: String, body: String, inputType: Int, positiveButton: String, positivelistener: (String) -> Unit) {
        val alert = AlertDialog.Builder(this).create()
        alert.setTitle(title)
        alert.setMessage(body)

        val dpi = getResources().getDisplayMetrics().density
        val input: EditText = EditText(this);
        input.inputType = inputType

        alert.setButton(AlertDialog.BUTTON_NEUTRAL, positiveButton, DialogInterface.OnClickListener { dialog, which ->
            val text = input.text.toString().trim()
            if(text.equals("")) {
                alert.dismiss()
                showDialogWithOneButton(getString(R.string.error_dialog_title),getString(R.string.error_default_empty_input_message_dialog), getString(R.string.error_dialog_button), DialogInterface.OnClickListener { dialog, which ->  })
            } else {
                positivelistener(input.text.toString())
            }
        })

        alert.setView(input, (19*dpi).toInt(), (5*dpi).toInt(), (14*dpi).toInt(), (5*dpi).toInt())
        alert.setCanceledOnTouchOutside(false)

        if (!isFinishing) {
            alert.show()
        }
    }

    fun speak(text: String, shouldStopTalking: Boolean = true) {
        if(shouldStopTalking) {
            ecoTalker?.stop()
        }
        ecoTalker?.speak(text, TextToSpeech.QUEUE_ADD, null, null)
    }

    fun initializeTextToSpeech(context: Context, onInitializeComplete: () -> Unit) {
        ecoTalker = TextToSpeech(context, TextToSpeech.OnInitListener { result ->
            if (result == TextToSpeech.SUCCESS) {
                ecoTalker?.setLanguage(Locale("es", "CO"))
                onInitializeComplete()
            }
        })
    }

    fun destroyTextToSpeech() {
        ecoTalker?.stop()
        ecoTalker?.shutdown()
        ecoTalker = null
    }

    // Intents
    fun goToActivity(context: Context, cls: Class<*>, shouldFinishCurrentClass: Boolean = false) {
        val intent = Intent(context, cls)
        startActivity(intent)
        if (shouldFinishCurrentClass) finish()
    }

    fun goToActivityWithExtraString(context: Context, cls: Class<*>, shouldFinishCurrentClass: Boolean = false, key: String, data: String) {
        val intent = Intent(context, cls)
        intent.putExtra(key, data)
        startActivity(intent)
        if (shouldFinishCurrentClass) finish()
    }

    fun goToActivityWithExtraStringArray(context: Context, cls: Class<*>, shouldFinishCurrentClass: Boolean = false, key: String, data: Array<String>) {
        val intent = Intent(context, cls)
        intent.putExtra(key, data)
        startActivity(intent)
        if (shouldFinishCurrentClass) finish()
    }

    fun finishActivityWithResult(resultCode: Int) {
        val intent = Intent()
        setResult(resultCode, intent)
        finish()
    }

    fun getExtraString(key: String): String {
        val intent: Intent = getIntent();
        val extra: String = intent.getStringExtra(key)
        return extra
    }

    fun getExtraOptionalString(key: String): String? {
        val intent: Intent = getIntent();
        if (intent.hasExtra(key)) {
            val extra: String = intent.getStringExtra(key)
            return extra
        } else {
            return null
        }
    }

    fun getExtraStringArray(key: String): Array<String> {
        val intent: Intent = getIntent();
        val extra: Array<String> = intent.getStringArrayExtra(key)
        return extra
    }

    fun getExtraOptionalStringArray(key: String): Array<String>? {
        val intent: Intent = getIntent();
        if (intent.hasExtra(key)) {
            val extra: Array<String> = intent.getStringArrayExtra(key)
            return extra
        } else {
            return null
        }
    }

    // Check Sensors
    fun userHasInternet(): Boolean {
        val cm = applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = cm.activeNetworkInfo
        val isConnected = activeNetwork != null && activeNetwork.isConnected
        return isConnected
    }

    fun userHasBluetooth(): Boolean {
        val mBluetoothAdapter: BluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
        return mBluetoothAdapter.isEnabled
    }

    fun userHasLocation(): Boolean {
        val lm = this.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return lm.isProviderEnabled(LocationManager.GPS_PROVIDER)
    }

    fun userHasMagnetometer(context: Context): Boolean {
        val sensorManager: SensorManager = context.getSystemService(Context.SENSOR_SERVICE) as SensorManager
        val listSensors: List<Sensor> = sensorManager.getSensorList(Sensor.TYPE_MAGNETIC_FIELD)
        return !listSensors.isEmpty()
    }

    fun userHasAccelerometer(context: Context): Boolean {
        val sensorManager: SensorManager = context.getSystemService(Context.SENSOR_SERVICE) as SensorManager
        val listSensors: List<Sensor> = sensorManager.getSensorList(Sensor.TYPE_ACCELEROMETER)
        return !listSensors.isEmpty()
    }

    // Check permissions
    fun askAppPermissions(onPermissionGiven: () -> Unit, onPermissionDenied: () -> Unit) {
        askPermission() {
            onPermissionGiven()
        }.onDeclined { e ->
            if (e.denied.size > 0) {
                val name: String = permissionToSimpleString(e.denied.get(0))
                showDialogWithOneButton(getString(R.string.permission_denied_dialog_title), "Sin el permiso $name no puedes usar todas las funcionalidades de la aplicación. Por favor comprueba que el permiso este habilitado.", getString(R.string.permission_denied_dialog_positive_button), DialogInterface.OnClickListener { dialog, which -> })
                onPermissionDenied()
            } else if (e.foreverDenied.size > 0) {
                val name: String = permissionToSimpleString(e.foreverDenied.get(0))
                showDialogWithOneButton(getString(R.string.permission_denied_dialog_title), "Sin el permiso $name no puedes usar todas las funcionalidades de la aplicación. Por favor comprueba que el permiso este habilitado.", getString(R.string.permission_denied_dialog_positive_button), DialogInterface.OnClickListener { dialog, which -> })
                onPermissionDenied()
            }
        }
    }

    fun permissionToSimpleString(permission: String): String {
        when (permission) {
            Manifest.permission.WRITE_EXTERNAL_STORAGE -> {
                return getString(R.string.permission_write_external_storage)
            }
            Manifest.permission.ACCESS_COARSE_LOCATION -> {
                return getString(R.string.permission_access_coarse_location)
            }
            Manifest.permission.RECORD_AUDIO -> {
                return getString(R.string.permission_record_audio)
            }
        }
        return ""
    }
}