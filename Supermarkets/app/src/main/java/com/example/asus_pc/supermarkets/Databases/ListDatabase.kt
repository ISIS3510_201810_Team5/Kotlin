package com.example.asus_pc.supermarkets.Databases

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import android.content.Context
import com.example.asus_pc.supermarkets.DAOS.ListEntityDAO
import com.example.asus_pc.supermarkets.Entities.ListEntity
import com.example.asus_pc.supermarkets.Helpers.StringListConverter

@Database(entities = arrayOf(ListEntity::class), version = 3)
@TypeConverters(StringListConverter::class)
abstract class ListDatabase : RoomDatabase() {

    abstract fun listEntityDao(): ListEntityDAO

    companion object {
        private var INSTANCE: ListDatabase? = null

        fun getInstance(context: Context): ListDatabase? {
            if (INSTANCE == null) {
                synchronized(ListDatabase::class) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            ListDatabase::class.java, "list.db")
                            .build()
                }
            }
            return INSTANCE
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }
}
