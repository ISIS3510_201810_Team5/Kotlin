package com.example.asus_pc.supermarkets.Models

import java.util.*

class Message {

    enum class STATUS {
        PENDING,
        COMING,
        DONE,
        CANCELLED
    }

    var createdAt: Date? = null
    val id : String
    val status : STATUS
    var atiende: String? = null
    var foto: String? = null

    constructor(createdAt: Date, id: String, status: Long, atiende: String, foto: String) {
        this.createdAt = createdAt
        this.id = id
        this.status = Message.longToStatusEnum(status)
        this.atiende = atiende
        this.foto = foto
    }

    constructor(id: String, data: Map<String, Any>) {
        this.id = id
        this.status = Message.longToStatusEnum(data.get("status") as Long)
        if(data.containsKey("atiende")) {
            val name = data.get("atiende") as String
            this.atiende = name.capitalize()
        }
        if(data.containsKey("foto")) {
            this.foto = data.get("foto") as String
        }
        if(data.containsKey("createdAt")) {
            this.createdAt = data.get("createdAt") as Date
        }
    }

    // Helpers
    companion object {
        fun longToStatusEnum(status: Long) : STATUS {
            when(status) {
                0L -> {
                    return STATUS.PENDING
                }
                1L -> {
                    return STATUS.COMING
                }
                2L -> {
                    return STATUS.DONE
                }
                3L -> {
                    return STATUS.CANCELLED
                }
            }
            return STATUS.PENDING
        }

        fun statusEnumToLong(status: STATUS) : Long {
            when(status) {
                STATUS.PENDING -> {
                    return 0
                }
                STATUS.COMING -> {
                    return 1
                }
                STATUS.DONE -> {
                    return 2
                }
                STATUS.CANCELLED -> {
                    return 3
                }
            }
        }
    }
}